# SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0 OR MIT

defmodule EmbeddedGraphics.Text.MonoText do
  alias EmbeddedGraphics.Nif
  alias EmbeddedGraphics.Drawable
  alias EmbeddedGraphics.Geometry
  alias EmbeddedGraphics.Geometry.Dimensions
  alias EmbeddedGraphics.Pixelcolor
  alias EmbeddedGraphics.Transform

  defmodule Style do
    alias EmbeddedGraphics.Nif
    alias EmbeddedGraphics.Pixelcolor

    use TypedStruct

    typedstruct do
      field(:inner, reference, enforce: true)
    end

    @spec new(color :: Pixelcolor.t(), font :: atom()) :: {:ok, t()} | {:error, atom()}
    def new(color, font) do
      with {:ok, inner} <- Nif.new_mono_text_style(color, font) do
        {:ok,
         %__MODULE__{
           inner: inner
         }}
      end
    end
  end

  use TypedStruct

  typedstruct do
    field(:inner, reference, enforce: true)
  end

  @spec new(text :: String.t(), position :: Geometry.point(), style :: Style) :: t()
  def new(text, position = {_x, _y}, %Style{inner: inner_style}) when is_binary(text) do
    %__MODULE__{
      inner: Nif.new_mono_text(text, position, inner_style)
    }
  end

  defimpl Dimensions do
    alias EmbeddedGraphics.Primitives.Rectangle

    def bounding_box(%{inner: inner}) do
      %Rectangle{
        inner: Nif.bounding_box_of_mono_text(inner)
      }
    end
  end

  defimpl Drawable do
    def draw(%{inner: inner}, target) do
      Nif.draw_mono_text(inner, target)
    end
  end

  defimpl Transform do
    alias EmbeddedGraphics.Text.MonoText

    def translate(%{inner: inner}, by) do
      %MonoText{
        inner: Nif.translate_mono_text(inner, by)
      }
    end
  end
end
