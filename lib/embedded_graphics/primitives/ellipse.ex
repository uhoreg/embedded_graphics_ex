# SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0 OR MIT

defmodule EmbeddedGraphics.Primitives.Ellipse do
  @moduledoc """
  Ellipse primitive

  # Examples

  ```
  alias EmbeddedGraphics.Primitives.Ellipse
  alias EmbeddedGraphics.Primitives.PrimitiveStyle
  alias EmbeddedGraphics.Primitives.Styled
  alias EmbeddedGraphics.Drawable
  alias EmbeddedGraphics.Transform

  # Ellipse with 1 pixel wide white stroke with top-left point at (10, 20) with a size of (30, 40)
  Ellipse.new({10, 20}, {30, 40})
  |> Styled.new(%PrimitiveStyle{stroke_color: :white, stroke_width: 1})
  |> Drawable.draw(display)

  # Ellipse with styled stroke and fill with top-left point at (20, 30) with a size of (40, 30)
  style = %PrimitiveStyle{
    stroke_color: :red,
    stroke_width: 3,
    fill_color: :green,
  }

  Ellipse.new({20, 30}, {40, 30})
  |> Styled.new(style)
  |> Drawable.draw(display)

  # Ellipse with blue fill and no stroke with a translation applied
  Ellipse.new({10, 20}, {20, 40})
  |> Transform.translate({10, -15})
  |> Styled.new(%PrimitiveStyle{fill_color: :blue})
  |> Drawable.draw(display)
  ```
  """
  alias EmbeddedGraphics.Nif
  alias EmbeddedGraphics.Geometry
  alias EmbeddedGraphics.Primitives.PrimitiveStyle

  use TypedStruct

  typedstruct opaque: true do
    field :inner, reference, enforce: true
  end

  @doc "Create a new ellipse delimited with a top-left point with a specific size"
  @spec new(top_left :: Geometry.point(), size :: Geometry.size()) :: t()
  def new(top_left = {_x, _y}, size = {_width, _height}) do
    %__MODULE__{
      inner: Nif.new_ellipse(top_left, size)
    }
  end

  @doc "Create a new ellipse centered around a given point with a specific size"
  @spec new_with_center(center :: Geometry.point(), size :: Geometry.size()) :: t()
  def new_with_center(center = {_x, _y}, size = {_width, _height}) do
    %__MODULE__{
      inner: Nif.new_ellipse_with_center(center, size)
    }
  end

  @doc "Top-left point of ellipse’s bounding box"
  @spec top_left(ellipse :: t()) :: Geometry.point()
  def top_left(_ellipse = %__MODULE__{inner: inner}) do
    Nif.top_left_of_ellipse(inner)
  end

  @doc "Size of the ellipse"
  @spec size(ellipse :: t()) :: Geometry.size()
  def size(_ellipse = %__MODULE__{inner: inner}) do
    Nif.size_of_ellipse(inner)
  end

  @doc "The center point of the ellipse"
  @spec center(ellipse :: t()) :: Geometry.point()
  def center(_ellipse = %__MODULE__{inner: inner}) do
    Nif.center_of_ellipse(inner)
  end

  defimpl EmbeddedGraphics.Eq do
    alias EmbeddedGraphics.Primitives.Ellipse

    def eq?(%{inner: inner1}, %Ellipse{inner: inner2}) do
      inner1 == inner2 or Nif.ellipse_eq(inner1, inner2)
    end

    def eq?(_, _) do
      false
    end
  end

  defimpl EmbeddedGraphics.Geometry.Dimensions do
    alias EmbeddedGraphics.Primitives.Rectangle

    def bounding_box(%{inner: inner}) do
      %Rectangle{
        inner: Nif.bounding_box_of_ellipse(inner)
      }
    end
  end

  defimpl EmbeddedGraphics.Primitives.ContainsPoint do
    def contains?(%{inner: inner}, point = {_x, _y}) do
      Nif.ellipse_contains_point(inner, point)
    end
  end

  defimpl EmbeddedGraphics.Primitives.Primitive do
    alias EmbeddedGraphics.Primitives.Styled
    defdelegate styled(primitive, style), to: Styled, as: :new
  end

  defimpl EmbeddedGraphics.Primitives.StyledDimensions do
    alias EmbeddedGraphics.Primitives.Rectangle

    def styled_bounding_box(%{inner: inner}, style = %PrimitiveStyle{}) do
      {:ok, bounding_box} = Nif.styled_bounding_box_of_ellipse(inner, style)

      %Rectangle{
        inner: bounding_box
      }
    end
  end

  defimpl EmbeddedGraphics.Primitives.StyledDrawable do
    alias EmbeddedGraphics.Primitives.PrimitiveStyle

    def draw_styled(%{inner: inner}, style = %PrimitiveStyle{}, target) do
      Nif.draw_styled_ellipse(inner, style, target)
    end
  end

  defimpl EmbeddedGraphics.Transform do
    alias EmbeddedGraphics.Primitives.Ellipse

    def translate(%{inner: inner}, by = {_x, _y}) do
      %Ellipse{
        inner: Nif.translate_ellipse(inner, by)
      }
    end
  end
end
