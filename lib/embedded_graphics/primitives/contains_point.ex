# SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0 OR MIT

defprotocol EmbeddedGraphics.Primitives.ContainsPoint do
  @moduledoc "Protocol to check if a point is inside a closed shape."
  alias EmbeddedGraphics.Geometry

  @doc "Returns `true` if the given point is inside the shape."
  @spec contains?(t(), point :: Geometry.point()) :: boolean()
  def contains?(primitive, point)
end
