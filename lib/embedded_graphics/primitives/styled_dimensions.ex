# SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0 OR MIT

defprotocol EmbeddedGraphics.Primitives.StyledDimensions do
  alias EmbeddedGraphics.Primitives.PrimitiveStyle
  alias EmbeddedGraphics.Primitives.Rectangle

  @spec styled_bounding_box(primitive :: t(), style :: PrimitiveStyle.t()) ::
          {:ok, Rectangle.t()} | {:error, atom}
  def styled_bounding_box(primitive, style)
end
