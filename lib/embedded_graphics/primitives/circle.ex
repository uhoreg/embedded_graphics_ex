# SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0 OR MIT

defmodule EmbeddedGraphics.Primitives.Circle do
  @moduledoc """
  Circle primitive

  # Examples

  ```
  alias EmbeddedGraphics.Primitives.Circle
  alias EmbeddedGraphics.Primitives.PrimitiveStyle
  alias EmbeddedGraphics.Primitives.Styled
  alias EmbeddedGraphics.Drawable
  alias EmbeddedGraphics.Transform

  # Circle with 1 pixel wide white stroke with top-left point at (10, 20) with a diameter of 30
  Circle.new({10, 20}, 30)
  |> Styled.new(%PrimitiveStyle{stroke_color: :white, stroke_width: 1})
  |> Drawable.draw(display)

  # Circle with styled stroke and fill with top-left point at (50, 20) with a diameter of 30
  style = %PrimitiveStyle{
    stroke_color: :red,
    stroke_width: 3,
    fill_color: :green,
  }

  Circle.new({50, 20}, 10)
  |> Styled.new(style)
  |> Drawable.draw(display)

  # Circle with blue fill and no stroke with a translation applied
  Circle.new({10, 20}, 30)
  |> Transform.translate({20, 10})
  |> Styled.new(%PrimitiveStyle{fill_color: :blue})
  |> Drawable.draw(display)
  ```
  """
  alias EmbeddedGraphics.Nif
  alias EmbeddedGraphics.Geometry
  alias EmbeddedGraphics.Primitives.PrimitiveStyle

  use TypedStruct

  typedstruct opaque: true do
    field :inner, reference, enforce: true
  end

  @doc "Create a new circle delimited with a top-left point with a specific diameter"
  @spec new(top_left :: Geometry.point(), diameter :: non_neg_integer()) :: t()
  def new(top_left = {_x, _y}, diameter) do
    %__MODULE__{
      inner: Nif.new_circle(top_left, diameter)
    }
  end

  @doc "Create a new circle centered around a given point with a specific diameter"
  @spec new_with_center(center :: Geometry.point(), diameter :: non_neg_integer()) :: t()
  def new_with_center(center = {_x, _y}, diameter) do
    %__MODULE__{
      inner: Nif.new_circle_with_center(center, diameter)
    }
  end

  @doc "Top-left point of circle’s bounding box"
  @spec top_left(circle :: t()) :: Geometry.point()
  def top_left(_circle = %__MODULE__{inner: inner}) do
    Nif.top_left_of_circle(inner)
  end

  @doc "Diameter of the circle"
  @spec diameter(circle :: t()) :: non_neg_integer()
  def diameter(_circle = %__MODULE__{inner: inner}) do
    Nif.diameter_of_circle(inner)
  end

  @doc "The center point of the circle"
  @spec center(circle :: t()) :: Geometry.point()
  def center(_circle = %__MODULE__{inner: inner}) do
    Nif.center_of_circle(inner)
  end

  defimpl EmbeddedGraphics.Eq do
    alias EmbeddedGraphics.Primitives.Circle

    def eq?(%{inner: inner1}, %Circle{inner: inner2}) do
      inner1 == inner2 or Nif.circle_eq(inner1, inner2)
    end

    def eq?(_, _) do
      false
    end
  end

  defimpl EmbeddedGraphics.Geometry.Dimensions do
    alias EmbeddedGraphics.Primitives.Rectangle

    def bounding_box(%{inner: inner}) do
      %Rectangle{
        inner: Nif.bounding_box_of_circle(inner)
      }
    end
  end

  defimpl EmbeddedGraphics.Primitives.ContainsPoint do
    def contains?(%{inner: inner}, point = {_x, _y}) do
      Nif.circle_contains_point(inner, point)
    end
  end

  defimpl EmbeddedGraphics.Primitives.Primitive do
    alias EmbeddedGraphics.Primitives.Styled
    defdelegate styled(primitive, style), to: Styled, as: :new
  end

  defimpl EmbeddedGraphics.Primitives.StyledDimensions do
    alias EmbeddedGraphics.Primitives.Rectangle

    def styled_bounding_box(%{inner: inner}, style = %PrimitiveStyle{}) do
      {:ok, bounding_box} = Nif.styled_bounding_box_of_circle(inner, style)

      %Rectangle{
        inner: bounding_box
      }
    end
  end

  defimpl EmbeddedGraphics.Primitives.StyledDrawable do
    alias EmbeddedGraphics.Primitives.PrimitiveStyle

    def draw_styled(%{inner: inner}, style = %PrimitiveStyle{}, target) do
      Nif.draw_styled_circle(inner, style, target)
    end
  end

  defimpl EmbeddedGraphics.Transform do
    alias EmbeddedGraphics.Primitives.Circle

    def translate(%{inner: inner}, by = {_x, _y}) do
      %Circle{
        inner: Nif.translate_circle(inner, by)
      }
    end
  end
end
