# SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0 OR MIT

defmodule EmbeddedGraphics.Primitives.PrimitiveStyle do
  use TypedStruct

  typedstruct do
    field(:fill_color, EmbeddedGraphics.Color.t() | nil)
    field(:stroke_color, EmbeddedGraphics.Color.t() | nil)
    field(:stroke_width, integer() | nil)
    field(:stroke_alignment, :inside | :center | :outside | nil)
  end
end
