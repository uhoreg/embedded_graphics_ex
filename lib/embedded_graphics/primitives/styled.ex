# SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0 OR MIT

defmodule EmbeddedGraphics.Primitives.Styled do
  alias EmbeddedGraphics.Primitives

  use TypedStruct

  typedstruct do
    field(:primitive, Primitives.t(), enforce: true)
    field(:style, Primitives.PrimitiveStyle.t(), enforce: true)
  end

  @spec new(primitive :: Primitives.t(), style :: Primitives.PrimitiveStyle.t()) :: t()
  def new(primitive, style) do
    %__MODULE__{primitive: primitive, style: style}
  end

  defimpl EmbeddedGraphics.Drawable do
    alias EmbeddedGraphics.Primitives.StyledDrawable

    def draw(drawable, target) do
      StyledDrawable.draw_styled(drawable.primitive, drawable.style, target)
    end
  end

  defimpl EmbeddedGraphics.Geometry.Dimensions do
    alias EmbeddedGraphics.Primitives.StyledDimensions

    def bounding_box(object) do
      StyledDimensions.styled_bounding_box(object.primitive, object.style)
    end
  end
end
