# SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0 OR MIT

defmodule EmbeddedGraphics.Primitives.Rectangle do
  @moduledoc """
  Rectangle primitive

  # Examples

  ```
  alias EmbeddedGraphics.Primitives.Rectangle
  alias EmbeddedGraphics.Primitives.PrimitiveStyle
  alias EmbeddedGraphics.Primitives.Styled
  alias EmbeddedGraphics.Drawable
  alias EmbeddedGraphics.Transform

  # Rectangle with red 3 pixel wide stroke and green fill with the top left corner at (30, 20) and
  # a size of (10, 15)
  style = %PrimitiveStyle{
    stroke_color: :red,
    stroke_width: 3
    fill_color: :green
  }

  Rectangle.new({30, 20}, {10, 15})
  |> Styled.new(style)
  |> Drawable.draw(display)

  # Rectangle with translation applied
  Rectangle.new({30, 20}, {10, 15})
  |> Transform.translate({-20, -10})
  |> Styled.new(style)
  |> Drawable.draw(display)
  ```
  """

  alias EmbeddedGraphics.Nif
  alias EmbeddedGraphics.Geometry
  alias EmbeddedGraphics.Primitives.PrimitiveStyle

  use TypedStruct

  typedstruct opaque: true do
    field(:inner, reference, enforce: true)
  end

  @doc "Creates a new rectangle from the top left point and the size."
  @spec new(top_left :: Geometry.point(), size :: Geometry.size()) :: t()
  def new(top_left = {_x, _y}, size = {_w, _h}) do
    %__MODULE__{
      inner: Nif.new_rectangle(top_left, size)
    }
  end

  @doc "Creates a new rectangle from two corners."
  @spec new_with_corners(corner_1 :: Geometry.point(), corner_2 :: Geometry.point()) :: t()
  def new_with_corners(corner_1 = {_x1, _y1}, corner_2 = {_x2, _y2}) do
    %__MODULE__{
      inner: Nif.new_rectangle_with_corners(corner_1, corner_2)
    }
  end

  @doc """
  Creates a new rectangle from the center point and the size.

  For rectangles with even width and/or height the top left corner doesn’t
  align with the pixel grid. Because of this the coordinates of the top left
  corner will be rounded up to the nearest integer coordinate.
  """
  @spec new_with_center(center :: Geometry.point(), size :: Geometry.size()) :: t()
  def new_with_center(center = {_x, _y}, size = {_w, _h}) do
    %__MODULE__{
      inner: Nif.new_rectangle_with_center(center, size)
    }
  end

  @doc "Top left point of the rectangle."
  @spec top_left(rectangle :: t()) :: Geometry.point()
  def top_left(_rectangle = %__MODULE__{inner: inner}) do
    Nif.top_left_of_rectangle(inner)
  end

  @doc "Size of the rectangle."
  @spec size(rectangle :: t()) :: Geometry.size()
  def size(_rectangle = %__MODULE__{inner: inner}) do
    Nif.size_of_rectangle(inner)
  end

  @doc """
  Returns the center of this rectangle.

  For rectangles with even width and/or height the returned value is rounded down to the nearest integer coordinate.
  """
  @spec center(rectangle :: t()) :: Geometry.point()
  def center(_rectangle = %__MODULE__{inner: inner}) do
    Nif.center_of_rectangle(inner)
  end

  @doc """
  Returns the bottom right corner of this rectangle.

  Because the smallest rectangle that can be represented by its corners has a size of 1 x 1 pixels, this function returns `nil` if the width or height of the rectangle is zero.
  """
  @spec bottom_right(rectanngle :: t()) :: Geometry.point() | nil
  def bottom_right(_rectangle = %__MODULE__{inner: inner}) do
    Nif.bottom_right_of_rectangle(inner)
  end

  @doc """
  Returns a new `Rectangle` containing the intersection of two rectangles.

  If no intersection is present, this method will return a zero sized rectangle.
  """
  @spec intersection(rect1 :: t(), rect2 :: t()) :: t()
  def intersection(_rect1 = %__MODULE__{inner: inner1}, _rect2 = %__MODULE__{inner: inner2}) do
    %__MODULE__{
      inner: Nif.intersection_of_rectangles(inner1, inner2)
    }
  end

  @doc """
  Returns a resized copy of this rectangle.

  The rectangle is resized relative to the given anchor point.
  """
  @spec resized(
          rectangle :: t(),
          size :: Geometry.size(),
          anchor_point :: Geometry.anchor_point()
        ) :: t()
  def resized(_rectangle = %__MODULE__{inner: inner}, size = {_w, _h}, anchor_point)
      when is_atom(anchor_point) do
    %__MODULE__{
      inner: Nif.resized_rectangle(inner, size, anchor_point)
    }
  end

  @doc "Returns a new rectangle with the given width, resized relative to the given anchor edge."
  @spec resized_width(
          rectangle :: t(),
          width :: non_neg_integer(),
          anchor_x :: Geometry.anchor_x()
        ) :: t()
  def resized_width(_rectangle = %__MODULE__{inner: inner}, width, anchor_x)
      when is_integer(width) and width >= 0 and is_atom(anchor_x) do
    %__MODULE__{
      inner: Nif.resized_width_rectangle(inner, width, anchor_x)
    }
  end

  @doc "Returns a new rectangle with the given height, resized relative to the given anchor edge."
  @spec resized_height(
          rectangle :: t(),
          height :: non_neg_integer(),
          anchor_y :: Geometry.anchor_y()
        ) :: t()
  def resized_height(_rectangle = %__MODULE__{inner: inner}, height, anchor_y)
      when is_integer(height) and height >= 0 and is_atom(anchor_y) do
    %__MODULE__{
      inner: Nif.resized_height_rectangle(inner, height, anchor_y)
    }
  end

  @doc """
  Offset the rectangle by a given value.

  Negative values will shrink the rectangle.
  """
  @spec offset(rectangle :: t(), offset: integer()) :: t()
  def offset(_rectangle = %__MODULE__{inner: inner}, offset) when is_integer(offset) do
    %__MODULE__{
      inner: Nif.offset_rectangle(inner, offset)
    }
  end

  @doc "Returns an anchor point."
  @spec anchor_point(rectangle :: t(), anchor_point :: Geometry.anchor_point()) ::
          Geometry.point()
  def anchor_point(_rectangle = %__MODULE__{inner: inner}, anchor_point)
      when is_atom(anchor_point) do
    Nif.anchor_point_of_rectangle(inner, anchor_point)
  end

  @doc "Returns the X coordinate of a given anchor edge of the rectangle."
  @spec anchor_x(rectangle :: t(), anchor_x :: Geometry.anchor_x()) :: non_neg_integer()
  def anchor_x(_rectangle = %__MODULE__{inner: inner}, anchor_x) when is_atom(anchor_x) do
    Nif.anchor_x_of_rectangle(inner, anchor_x)
  end

  @doc "Returns the Y coordinate of a given anchor edge of the rectangle."
  @spec anchor_y(rectangle :: t(), anchor_y :: Geometry.anchor_y()) :: non_neg_integer()
  def anchor_y(_rectangle = %__MODULE__{inner: inner}, anchor_y) when is_atom(anchor_y) do
    Nif.anchor_y_of_rectangle(inner, anchor_y)
  end

  @doc "Returns the range of Y coordinates in this rectangle."
  @spec rows(rectangle :: t()) :: Range.t()
  def rows(_rectangle = %__MODULE__{inner: inner}) do
    {start_row, end_row} = Nif.rows_of_rectangle(inner)
    # The Rust function returns the endpoints of the range.  A range in Rust
    # excludes the end of the range, whereas a range in Elixir includes the end
    # of the range, so we need to subtract 1.  We need to tell the range that
    # the step is 1, because otherwise, a zero-sized range (e.g. 0..-1) will
    # have a step of -1.
    Range.new(start_row, end_row - 1, 1)
  end

  @doc "Returns the range of X coordinates in this rectangle."
  @spec columns(rectangle :: t()) :: Range.t()
  def columns(_rectangle = %__MODULE__{inner: inner}) do
    {start_col, end_col} = Nif.columns_of_rectangle(inner)
    Range.new(start_col, end_col - 1, 1)
  end

  @doc """
  Returns true is the rectangle is zero sized.

  A rectangle is zero sized if the width or height are zero.
  """
  @spec is_zero_sized?(rectangle :: t()) :: boolean()
  def is_zero_sized?(_rectangle = %__MODULE__{inner: inner}) do
    Nif.is_rectangle_zero_sized(inner)
  end

  defimpl EmbeddedGraphics.Eq do
    alias EmbeddedGraphics.Primitives.Rectangle

    def eq?(%{inner: inner1}, %Rectangle{inner: inner2}) do
      inner1 == inner2 or Nif.rectangle_eq(inner1, inner2)
    end

    def eq?(_, _) do
      false
    end
  end

  defimpl EmbeddedGraphics.Geometry.Dimensions do
    alias EmbeddedGraphics.Primitives.Rectangle

    def bounding_box(%{inner: inner}) do
      %Rectangle{
        inner: Nif.bounding_box_of_rectangle(inner)
      }
    end
  end

  defimpl EmbeddedGraphics.Primitives.ContainsPoint do
    def contains?(%{inner: inner}, point = {_x, _y}) do
      Nif.rectangle_contains_point(inner, point)
    end
  end

  defimpl EmbeddedGraphics.Primitives.Primitive do
    alias EmbeddedGraphics.Primitives.Styled
    defdelegate styled(primitive, style), to: Styled, as: :new
  end

  defimpl EmbeddedGraphics.Primitives.StyledDimensions do
    alias EmbeddedGraphics.Primitives.Rectangle

    def styled_bounding_box(%{inner: inner}, style = %PrimitiveStyle{}) do
      {:ok, bounding_box} = Nif.styled_bounding_box_of_rectangle(inner, style)

      %Rectangle{
        inner: bounding_box
      }
    end
  end

  defimpl EmbeddedGraphics.Primitives.StyledDrawable do
    alias EmbeddedGraphics.Primitives.PrimitiveStyle

    def draw_styled(
          _rectangle = %{inner: inner},
          style = %PrimitiveStyle{},
          target
        ) do
      Nif.draw_styled_rectangle(inner, style, target)
    end
  end

  defimpl EmbeddedGraphics.Transform do
    alias EmbeddedGraphics.Primitives.Rectangle

    def translate(%{inner: inner}, by = {_x, _y}) do
      %Rectangle{
        inner: Nif.translate_rectangle(inner, by)
      }
    end
  end
end
