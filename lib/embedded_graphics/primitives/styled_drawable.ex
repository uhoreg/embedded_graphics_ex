# SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0 OR MIT

defprotocol EmbeddedGraphics.Primitives.StyledDrawable do
  alias EmbeddedGraphics.DrawTarget
  alias EmbeddedGraphics.Primitives.PrimitiveStyle

  @spec draw_styled(primitive :: t(), style :: PrimitiveStyle.t(), target :: DrawTarget.t()) ::
          {:ok, {}} | {:error, atom}
  def draw_styled(primitive, style, target)
end
