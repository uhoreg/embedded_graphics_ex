# SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0 OR MIT

defprotocol EmbeddedGraphics.Primitives.Primitive do
  alias EmbeddedGraphics.Primitives.PrimitiveStyle
  alias EmbeddedGraphics.Primitives.Styled

  @spec styled(primitive :: t(), style :: PrimitiveStyle.t()) :: Styled.t()
  def styled(primitive, style)
end
