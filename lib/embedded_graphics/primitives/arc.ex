# SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0 OR MIT

defmodule EmbeddedGraphics.Primitives.Arc do
  @moduledoc """
  Arc primitive

  # Examples

  ```
  alias EmbeddedGraphics.Primitives.Arc
  alias EmbeddedGraphics.Primitives.PrimitiveStyle
  alias EmbeddedGraphics.Primitives.Styled
  alias EmbeddedGraphics.Drawable

  # Arc with 1 pixel wide white stroke with top-left point at (10, 20) with a diameter of 30
  Arc.new({10, 20}, 30, {0.0, :deg}, {90.0, :deg})
  |> Styled.new(%PrimitiveStyle{stroke_color: :white, stroke_width: 1})
  |> Drawable.draw(display)

  # Arc with styled stroke with top-left point at (15, 25) with a diameter of 20
  style = %PrimitiveStyle{
    stroke_color: :red,
    stroke_width: 3
  }

  Arc.new({15, 25}, 20, {180.0, :deg}, {-90.0, :deg})
  |> Styled.new(style)
  |> Drawable.draw(display)
  ```
  """

  alias EmbeddedGraphics.Nif
  alias EmbeddedGraphics.Geometry
  alias EmbeddedGraphics.Primitives.PrimitiveStyle
  alias EmbeddedGraphics.Primitives.Circle

  use TypedStruct

  typedstruct opaque: true do
    field :inner, reference, enforce: true
  end

  @doc "Create a new arc delimited with a top-left point with a specific diameter and start and sweep angles"
  @spec new(
          top_left :: Geometry.point(),
          diameter :: non_neg_integer(),
          angle_start :: Geometry.angle(),
          angle_sweep :: Geometry.angle()
        ) :: t()
  def new(top_left = {_x, _y}, diameter, angle_start, angle_sweep) do
    %__MODULE__{
      inner: Nif.new_arc(top_left, diameter, angle_start, angle_sweep)
    }
  end

  @doc "Create a new arc centered around a given point with a specific diameter and start and sweep angles"
  @spec new_with_center(
          center :: Geometry.point(),
          diameter :: non_neg_integer(),
          angle_start :: Geometry.angle(),
          angle_sweep :: Geometry.angle()
        ) :: t()
  def new_with_center(center = {_x, _y}, diameter, angle_start, angle_sweep) do
    %__MODULE__{
      inner: Nif.new_arc_with_center(center, diameter, angle_start, angle_sweep)
    }
  end

  @doc """
  Creates an arc based on a circle.

  The resulting arc will match the `top_left` and `diameter` of the base circle.
  """
  @spec new_from_circle(
          circle :: Circle.t(),
          angle_start :: Geometry.angle(),
          angle_sweep :: Geometry.angle()
        ) :: t()
  def new_from_circle(_circle = %Circle{inner: inner}, angle_start, angle_sweep) do
    %__MODULE__{
      inner: Nif.new_arc_from_circle(inner, angle_start, angle_sweep)
    }
  end

  @doc "Top-left point of the bounding-box of the circle supporting the arc"
  @spec top_left(t()) :: Geometry.point()
  def top_left(_arc = %__MODULE__{inner: inner}) do
    Nif.top_left_of_arc(inner)
  end

  @doc "Diameter of the circle supporting the arc"
  @spec diameter(t()) :: non_neg_integer()
  def diameter(_arc = %__MODULE__{inner: inner}) do
    Nif.diameter_of_arc(inner)
  end

  @doc "Angle at which the arc starts"
  @spec angle_start(t()) :: Geometry.angle()
  def angle_start(_arc = %__MODULE__{inner: inner}) do
    Nif.angle_start_of_arc(inner)
  end

  @doc "Angle defining the arc sweep starting at `angle_start`"
  @spec angle_sweep(t()) :: Geometry.angle()
  def angle_sweep(_arc = %__MODULE__{inner: inner}) do
    Nif.angle_sweep_of_arc(inner)
  end

  @doc "Return the center point of the arc."
  @spec center(t()) :: Geometry.point()
  def center(_arc = %__MODULE__{inner: inner}) do
    Nif.center_of_arc(inner)
  end

  @doc "Returns a circle with the same `top_left` and `diameter` as this arc."
  @spec to_circle(t()) :: Circle.t()
  def to_circle(_arc = %__MODULE__{inner: inner}) do
    %Circle{
      inner: Nif.arc_to_circle(inner)
    }
  end

  defimpl EmbeddedGraphics.Eq do
    alias EmbeddedGraphics.Primitives.Arc

    def eq?(%{inner: inner1}, %Arc{inner: inner2}) do
      inner1 == inner2 or Nif.arc_eq(inner1, inner2)
    end

    def eq?(_, _) do
      false
    end
  end

  defimpl EmbeddedGraphics.Geometry.Dimensions do
    alias EmbeddedGraphics.Primitives.Rectangle

    def bounding_box(%{inner: inner}) do
      %Rectangle{
        inner: Nif.bounding_box_of_arc(inner)
      }
    end
  end

  defimpl EmbeddedGraphics.Primitives.Primitive do
    alias EmbeddedGraphics.Primitives.Styled
    defdelegate styled(primitive, style), to: Styled, as: :new
  end

  defimpl EmbeddedGraphics.Primitives.StyledDimensions do
    alias EmbeddedGraphics.Primitives.Rectangle

    def styled_bounding_box(%{inner: inner}, style = %PrimitiveStyle{}) do
      {:ok, bounding_box} = Nif.styled_bounding_box_of_arc(inner, style)

      %Rectangle{
        inner: bounding_box
      }
    end
  end

  defimpl EmbeddedGraphics.Primitives.StyledDrawable do
    alias EmbeddedGraphics.Primitives.PrimitiveStyle

    def draw_styled(
          %{inner: inner},
          style = %PrimitiveStyle{},
          target
        ) do
      Nif.draw_styled_arc(inner, style, target)
    end
  end

  defimpl EmbeddedGraphics.Transform do
    alias EmbeddedGraphics.Primitives.Arc

    def translate(%{inner: inner}, by = {_x, _y}) do
      %Arc{
        inner: Nif.translate_arc(inner, by)
      }
    end
  end
end
