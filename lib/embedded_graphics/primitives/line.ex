# SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0 OR MIT

defmodule EmbeddedGraphics.Primitives.Line do
  @moduledoc """
  Line primitive

  # Examples

  ```
  alias EmbeddedGraphics.Primitives.Line
  alias EmbeddedGraphics.Primitives.PrimitiveStyle
  alias EmbeddedGraphics.Primitives.Styled
  alias EmbeddedGraphics.Drawable
  alias EmbeddedGraphics.Transform

  # Red 1 pixel wide line from (50, 20) to (60, 35)
  Line.new({50, 20}, {60, 35})
  |> Styled.new(%PrimitiveStyle{stroke_color: :red, stroke_width: 1})
  |> Drawable.draw(display)

  # Green 10 pixel wide line with translation applied
  Line.new({50, 20}, {60, 35})
  |> Transform.translate({-30, 10})
  |> Styled.new(%PrimitiveStyle{stroke_color: :green, stroke_width: 10})
  |> Drawable.draw(display)
  ```
  """
  alias EmbeddedGraphics.Nif
  alias EmbeddedGraphics.Geometry
  alias EmbeddedGraphics.Primitives.PrimitiveStyle

  use TypedStruct

  typedstruct opaque: true do
    field(:inner, reference, enforce: true)
  end

  @doc "Creates a line between two points"
  @spec new(start_point :: Geometry.point(), end_point :: Geometry.point()) :: t()
  def new(start_point = {_x1, _y1}, end_point = {_x2, _y2}) do
    %__MODULE__{
      inner: Nif.new_line(start_point, end_point)
    }
  end

  @doc "Creates a line with a start point and a delta vector"
  @spec new_with_delta(start :: Geometry.point(), delta :: Geometry.point()) :: t()
  def new_with_delta(start = {_x1, _y1}, delta = {_x2, _y2}) do
    %__MODULE__{
      inner: Nif.new_line_with_delta(start, delta)
    }
  end

  @doc "Start point of the line"
  @spec start_point(line :: t()) :: Geometry.point()
  def start_point(_line = %__MODULE__{inner: inner}) do
    Nif.start_of_line(inner)
  end

  @doc "End point of the line"
  @spec end_point(line :: t()) :: Geometry.point()
  def end_point(_line = %__MODULE__{inner: inner}) do
    Nif.end_of_line(inner)
  end

  @doc "Compute the midpoint of the line"
  @spec midpoint(line :: t()) :: Geometry.point()
  def midpoint(_line = %__MODULE__{inner: inner}) do
    Nif.midpoint_of_line(inner)
  end

  @doc "Compute the delta (`end - start`) of the line"
  @spec delta(line :: t()) :: Geometry.point()
  def delta(_line = %__MODULE__{inner: inner}) do
    Nif.delta_of_line(inner)
  end

  defimpl EmbeddedGraphics.Eq do
    alias EmbeddedGraphics.Primitives.Line

    def eq?(%{inner: inner1}, %Line{inner: inner2}) do
      inner1 == inner2 or Nif.line_eq(inner1, inner2)
    end

    def eq?(_, _) do
      false
    end
  end

  defimpl EmbeddedGraphics.Geometry.Dimensions do
    alias EmbeddedGraphics.Primitives.Rectangle

    def bounding_box(%{inner: inner}) do
      %Rectangle{
        inner: Nif.bounding_box_of_line(inner)
      }
    end
  end

  defimpl EmbeddedGraphics.Primitives.Primitive do
    alias EmbeddedGraphics.Primitives.Styled
    defdelegate styled(primitive, style), to: Styled, as: :new
  end

  defimpl EmbeddedGraphics.Primitives.StyledDimensions do
    alias EmbeddedGraphics.Primitives.Rectangle

    def styled_bounding_box(%{inner: inner}, style = %PrimitiveStyle{}) do
      {:ok, bounding_box} = Nif.styled_bounding_box_of_line(inner, style)

      %Rectangle{
        inner: bounding_box
      }
    end
  end

  defimpl EmbeddedGraphics.Primitives.StyledDrawable do
    alias EmbeddedGraphics.Primitives.PrimitiveStyle

    def draw_styled(
          %{inner: inner},
          style = %PrimitiveStyle{},
          target
        ) do
      Nif.draw_styled_line(inner, style, target)
    end
  end

  defimpl EmbeddedGraphics.Transform do
    alias EmbeddedGraphics.Primitives.Line

    def translate(%{inner: inner}, by = {_x, _y}) do
      %Line{
        inner: Nif.translate_line(inner, by)
      }
    end
  end
end
