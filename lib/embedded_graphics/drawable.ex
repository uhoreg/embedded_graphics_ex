# SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0 OR MIT

defprotocol EmbeddedGraphics.Drawable do
  @spec draw(t(), EmbeddedGraphics.DrawTarget.t()) :: {:ok, {}}
  def draw(drawable, target)
end
