# SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0 OR MIT

defmodule EmbeddedGraphics.Nif do
  @moduledoc false
  use Rustler, otp_app: :embedded_graphics, crate: "embedded_graphics_rustler"

  require EmbeddedGraphics.Macros

  # display/draw_target functions
  EmbeddedGraphics.Macros.iffeature "mock" do
    def new_display(), do: error()
    def display_set_allow_overdraw(_display, _value), do: error()
    def display_eq(_display1, _display2), do: error()
    def affected_area_of_display(_display), do: error()
    def display_diff_pattern(_display, _pattern), do: error()
    def bounding_box_of_display(_display), do: error()
    # stub functions
    def update_window(), do: error()
  end

  EmbeddedGraphics.Macros.iffeature "simulator" do
    def new_display(_display1, _display2), do: error()
    def update_window(_display), do: error()
    def bounding_box_of_display(_display), do: error()
    # stub functions
    def affected_area_of_display(), do: error()
    def display_eq(), do: error()
    def display_diff_pattern(), do: error()
    def display_set_allow_overdraw(), do: error()
  end

  def clear_display(_display_target, _color), do: error()

  # arc
  def new_arc(_top_left, _diameter, _angle_start, _angle_sweep), do: error()
  def new_arc_with_center(_center, _diameter, _angle_start, _angle_sweep), do: error()
  def new_arc_from_circle(_circle, _angle_start, _angle_sweep), do: error()
  def arc_to_circle(_arc), do: error()
  def top_left_of_arc(_arc), do: error()
  def diameter_of_arc(_arc), do: error()
  def angle_start_of_arc(_arc), do: error()
  def angle_sweep_of_arc(_arc), do: error()
  def center_of_arc(_arc), do: error()
  def arc_eq(_arc1, _arc2), do: error()
  def bounding_box_of_arc(_arc), do: error()
  def styled_bounding_box_of_arc(_arc, _style), do: error()
  def draw_styled_arc(_arc, _style, _display_target), do: error()
  def translate_arc(_arc, _by), do: error()

  # circle
  def new_circle(_top_left, _diameter), do: error()
  def new_circle_with_center(_center, _diameter), do: error()
  def top_left_of_circle(_circle), do: error()
  def center_of_circle(_circle), do: error()
  def diameter_of_circle(_circle), do: error()
  def circle_eq(_circle1, _circle2), do: error()
  def bounding_box_of_circle(_circle), do: error()
  def circle_contains_point(_circle, _point), do: error()
  def styled_bounding_box_of_circle(_circle, _style), do: error()
  def draw_styled_circle(_circle, _style, _display_target), do: error()
  def translate_circle(_circle, _by), do: error()

  # ellipse
  def new_ellipse(_top_left, _size), do: error()
  def new_ellipse_with_center(_center, _size), do: error()
  def top_left_of_ellipse(_ellipse), do: error()
  def center_of_ellipse(_ellipse), do: error()
  def size_of_ellipse(_ellipse), do: error()
  def ellipse_eq(_ellipse1, _ellipse2), do: error()
  def bounding_box_of_ellipse(_ellipse), do: error()
  def ellipse_contains_point(_ellipse, _point), do: error()
  def styled_bounding_box_of_ellipse(_ellipse, _style), do: error()
  def draw_styled_ellipse(_ellipse, _style, _display_target), do: error()
  def translate_ellipse(_ellipse, _by), do: error()

  # line
  def new_line(_start, _end), do: error()
  def new_line_with_delta(_start, _delta), do: error()
  def start_of_line(_line), do: error()
  def end_of_line(_line), do: error()
  def midpoint_of_line(_line), do: error()
  def delta_of_line(_line), do: error()
  def line_eq(_line1, _line2), do: error()
  def bounding_box_of_line(_line), do: error()
  def styled_bounding_box_of_line(_line, _style), do: error()
  def draw_styled_line(_line, _style, _display_target), do: error()
  def translate_line(_line, _by), do: error()

  # rectangle
  def new_rectangle(_top_left, _size), do: error()
  def new_rectangle_with_corners(_corner1, _corner2), do: error()
  def new_rectangle_with_center(_center, _size), do: error()
  def top_left_of_rectangle(_rectangle), do: error()
  def size_of_rectangle(_rectangle), do: error()
  def center_of_rectangle(_rectangle), do: error()
  def bottom_right_of_rectangle(_rectangle), do: error()
  def intersection_of_rectangles(_rectangle1, _rectangle2), do: error()
  def resized_rectangle(_rectangle, _size, _anchor_point), do: error()
  def resized_width_rectangle(_rectangle, _width, _anchor_x), do: error()
  def resized_height_rectangle(_rectangle, _height, _anchor_y), do: error()
  def offset_rectangle(_rectangle, _offset), do: error()
  def anchor_point_of_rectangle(_rectangle, _anchor_point), do: error()
  def anchor_x_of_rectangle(_rectangle, _anchor_x), do: error()
  def anchor_y_of_rectangle(_rectangle, _anchor_y), do: error()
  def rows_of_rectangle(_rectangle), do: error()
  def columns_of_rectangle(_rectangle), do: error()
  def is_rectangle_zero_sized(_rectangle), do: error()
  def rectangle_eq(_rectangle1, _rectangle2), do: error()
  def bounding_box_of_rectangle(_rectangle), do: error()
  def rectangle_contains_point(_rectangle, _point), do: error()
  def styled_bounding_box_of_rectangle(_rectangle, _style), do: error()
  def draw_styled_rectangle(_rectangle, _style, _display_target), do: error()
  def translate_rectangle(_rectangle, _by), do: error()

  # mono text
  def new_mono_text_style(_color, _font), do: error()
  def new_mono_text(_text, _position, _style), do: error()
  def bounding_box_of_mono_text(_text), do: error()
  def draw_mono_text(_text, _display_target), do: error()
  def translate_mono_text(_text, _by), do: error()

  defp error(), do: :erlang.nif_error(:nif_not_loaded)
end
