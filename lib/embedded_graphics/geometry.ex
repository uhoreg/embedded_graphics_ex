# SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0 OR MIT

defmodule EmbeddedGraphics.Geometry do
  @moduledoc "Geometry module"

  @typedoc "Angle in degrees or radians"
  @type angle :: {number, :deg} | {number, :rad}

  @typedoc """
  2D point.

  The point is represented by a tuple of the form `{x, y}`.
  """
  @type point :: {integer, integer}

  @doc "The x-coordinate of a point."
  @spec point_x(point :: point()) :: integer()
  def point_x(_point = {x, _}), do: x

  @doc "The y-coordinate of a point."
  @spec point_y(point :: point()) :: integer()
  def point_y(_point = {_, y}), do: y

  @doc """
  Macro to help with performing arithmetic on 2-dimensional points and sizes

  # Examples

      math_2d({1, 2} + {3, 4})  # {4, 6}

      math_d2({2, 4} / 2)  # {1, 2}

      math_d2(2 * {1, 2})  # {2, 4}
  """
  defmacro math_2d({:+, _data, [a, b]}) do
    quote bind_quoted: [a: a, b: b] do
      EmbeddedGraphics.Geometry.add(a, b)
    end
  end

  defmacro math_2d({:-, _data, [a, b]}) do
    quote bind_quoted: [a: a, b: b] do
      EmbeddedGraphics.Geometry.sub(a, b)
    end
  end

  defmacro math_2d({:-, _data, [a]}) do
    quote bind_quoted: [a: a] do
      EmbeddedGraphics.Geometry.neg(a)
    end
  end

  defmacro math_2d({:*, _data, [a, b]}) do
    quote bind_quoted: [a: a, b: b] do
      EmbeddedGraphics.Geometry.mul(a, b)
    end
  end

  defmacro math_2d({:/, _data, [a, b]}) do
    quote bind_quoted: [a: a, b: b] do
      EmbeddedGraphics.Geometry.div(a, b)
    end
  end

  @doc false
  def neg({x, y}), do: {-x, -y}
  def neg(a) when is_number(a), do: -a

  @doc false
  def add({x1, y1}, {x2, y2}), do: {x1 + x2, y1 + y2}
  def add(a, b) when is_number(a) and is_number(b), do: a + b

  @doc false
  def sub({x1, y1}, {x2, y2}), do: {x1 - x2, y1 - y2}
  def sub(a, b) when is_number(a) and is_number(b), do: a - b

  @doc false
  def mul({x, y}, z), do: {x * z, y * z}
  def mul(z, {x, y}), do: {z * x, z * y}
  def mul(a, b) when is_number(a) and is_number(b), do: a * b

  @doc false
  def div({x, y}, z), do: {Kernel.div(x, z), Kernel.div(y, z)}
  def div(a, b) when is_number(a) and is_number(b), do: Kernel.div(a, b)

  @typedoc """
  2D size.

  The size is represented by a tuple of the form `{width, height}`.
  """
  @type size :: {non_neg_integer, non_neg_integer}

  @doc "The width of a size."
  @spec size_width(size :: size()) :: non_neg_integer()
  def size_width(_point = {w, _}), do: w

  @doc "The height of a size."
  @spec size_height(size :: size()) :: non_neg_integer()
  def size_height(_point = {_, h}), do: h

  @typedoc "Anchor point."
  @type anchor_point ::
          :top_left
          | :top_center
          | :top_right
          | :center_left
          | :center
          | :center_right
          | :bottom_left
          | :bottom_center
          | :bottom_right

  @typedoc "X axis anchor point."
  @type anchor_x :: :left | :center | :right

  @typedoc "Y axis anchor point."
  @type anchor_y :: :top | :center | :bottom

  @doc "Creates an anchor point from an X and Y component."
  @spec anchor_point_from_xy(anchor_x :: anchor_x(), anchor_y :: anchor_y()) :: anchor_point()
  def anchor_point_from_xy(anchor_x, anchor_y) when is_atom(anchor_x) and is_atom(anchor_y) do
    case {anchor_x, anchor_y} do
      {:top, :left} -> :top_left
      {:top, :center} -> :top_center
      {:top, :right} -> :top_right
      {:center, :left} -> :center_left
      {:center, :center} -> :center
      {:center, :right} -> :center_right
      {:bottom, :left} -> :bottom_left
      {:bottom, :right} -> :bottom_right
    end
  end

  @doc "Returns the X axis component"
  @spec anchor_point_x(anchor_point :: anchor_point()) :: anchor_x()
  def anchor_point_x(anchor_point) when is_atom(anchor_point) do
    case anchor_point do
      :top_left -> :left
      :top_center -> :center
      :top_right -> :right
      :center_left -> :left
      :center -> :center
      :center_right -> :right
      :bottom_left -> :left
      :bottom_center -> :center
      :bottom_right -> :right
    end
  end

  @doc "Returns the Y axis component"
  @spec anchor_point_y(anchor_point :: anchor_point()) :: anchor_y()
  def anchor_point_y(anchor_point) when is_atom(anchor_point) do
    case anchor_point do
      :top_left -> :top
      :top_center -> :top
      :top_right -> :top
      :center_left -> :center
      :center -> :center
      :center_right -> :center
      :bottom_left -> :bottom
      :bottom_center -> :bottom
      :bottom_right -> :bottom
    end
  end
end
