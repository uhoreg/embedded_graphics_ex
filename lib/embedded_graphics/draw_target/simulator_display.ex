# SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0 OR MIT

defmodule EmbeddedGraphics.DrawTarget.SimulatorDisplay do
  @moduledoc """
  The embedded-graphics simulator, used for testing and debugging.
  """

  alias EmbeddedGraphics.Nif

  use TypedStruct

  typedstruct opaque: true do
    field(:inner, reference, enforce: true)
  end

  require EmbeddedGraphics.Macros

  EmbeddedGraphics.Macros.iffeature "simulator" do
    def new(size, window_title) do
      Nif.new_display(size, window_title)
    end

    def update_window(target = %__MODULE__{}) do
      Nif.update_window(target)
    end

    defimpl EmbeddedGraphics.DrawTarget do
      def clear(target, color) do
        Nif.clear_display(target, color)
      end
    end

    defimpl EmbeddedGraphics.Geometry.Dimensions do
      alias EmbeddedGraphics.Primitives.Rectangle

      def bounding_box(target) do
        %Rectangle{
          inner: Nif.bounding_box_of_display(target)
        }
      end
    end
  else
    def new(_size, _window_title), do: error()

    def update_window(_display_target), do: error()

    defp error(), do: raise(RuntimeError, message: "Not compiled to use simulator display")
  end
end
