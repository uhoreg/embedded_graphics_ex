# SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0 OR MIT

defmodule EmbeddedGraphics.DrawTarget.MockDisplay do
  @moduledoc """
  Mock display for use in tests.
  """

  alias EmbeddedGraphics.Nif
  alias EmbeddedGraphics.Primitives.Rectangle

  use TypedStruct

  typedstruct opaque: true do
    field(:inner, reference, enforce: true)
  end

  require EmbeddedGraphics.Macros

  EmbeddedGraphics.Macros.iffeature "mock" do
    @doc "Create a new mock display"
    @spec new() :: t()
    def new() do
      Nif.new_display()
    end

    @doc """
    Sets if overdrawing is allowed.

    If this is set to `true` the overdrawing is allowed.
    """
    @spec set_allow_overdraw(draw_target :: t(), value :: boolean) :: nil
    def set_allow_overdraw(draw_target = %__MODULE__{}, value) when is_boolean(value) do
      Nif.display_set_allow_overdraw(draw_target, value)
    end

    @doc """
    Diff the mock display against the given pattern

    If the mock display and the pattern differ, the diff will show a

    - "+" where the pattern has a pixel set but the mock display does not
    - "-" where the mock display has a pixel set but the pattern does not
    - "X" where the mock display and pattern are set to different colors

    """
    @spec diff_pattern(draw_target :: t(), pattern :: list(String.t())) :: nil | list(String.t())
    def diff_pattern(draw_target = %__MODULE__{}, pattern) do
      Nif.display_diff_pattern(draw_target, pattern)
    end

    @doc "Returns the area that was affected by drawing operations."
    @spec affected_area(draw_target :: t()) :: Rectangle.t()
    def affected_area(draw_target = %__MODULE__{}) do
      %Rectangle{
        inner: Nif.affected_area_of_display(draw_target)
      }
    end

    defimpl EmbeddedGraphics.DrawTarget do
      def clear(target, color) do
        Nif.clear_display(target, color)
      end
    end

    defimpl EmbeddedGraphics.Eq do
      alias EmbeddedGraphics.DrawTarget.MockDisplay

      def eq?(target1, target2 = %MockDisplay{}) do
        target1.inner == target2.inner or Nif.display_eq(target1, target2)
      end

      def eq?(_, _) do
        false
      end
    end

    defimpl EmbeddedGraphics.Geometry.Dimensions do
      alias EmbeddedGraphics.Primitives.Rectangle

      def bounding_box(target) do
        %Rectangle{
          inner: Nif.bounding_box_of_display(target)
        }
      end
    end
  else
    @doc "Create a new mock display"
    @spec new() :: t()
    def new(), do: error()

    @doc """
    Diff the mock display against the given pattern

    If the mock display and the pattern differ, the diff will show a

    - "+" where the pattern has a pixel set but the mock display does not
    - "-" where the mock display has a pixel set but the pattern does not
    - "X" where the mock display and pattern are set to different colors

    """
    @spec diff_pattern(draw_target :: t(), pattern :: list(String.t())) :: nil | list(String.t())
    def diff_pattern(_draw_target, _pattern), do: error()

    @doc """
    Sets if overdrawing is allowed.

    If this is set to `true` the overdrawing is allowed.
    """
    @spec set_allow_overdraw(draw_target :: t(), value :: boolean) :: nil
    def set_allow_overdraw(_draw_target, value), do: error()

    @doc "Returns the area that was affected by drawing operations."
    @spec affected_area(draw_target :: t()) :: Rectangle.t()
    def affected_area(_draw_target), do: error()

    defp error(), do: raise(RuntimeError, message: "Not compiled to use mock display")
  end
end
