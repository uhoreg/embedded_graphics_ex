# SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0 OR MIT

defprotocol EmbeddedGraphics.DrawTarget do
  @moduledoc """
  A target for embedded-graphics drawing operations.
  """

  alias EmbeddedGraphics.Pixelcolor

  @doc "Clear the display using the given color"
  @spec clear(t(), Pixelcolor.t()) :: {:ok, {}} | {:error, atom()}
  def clear(draw_target, color)
end
