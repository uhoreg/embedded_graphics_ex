# SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0 OR MIT

defprotocol EmbeddedGraphics.Geometry.Dimensions do
  @spec bounding_box(object :: t()) :: EmbeddedGraphics.Primitives.Rectangle.t()
  def bounding_box(object)
end
