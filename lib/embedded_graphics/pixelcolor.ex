# SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0 OR MIT

defmodule EmbeddedGraphics.Pixelcolor do
  @moduledoc """
  Indicates the data type to be used for pixel colors.

  All the color types are documented here, but only the one specified at compile
  time can be used.
  """

  @typedoc "A binary color (e.g. black and white).  `true` means 'on' and `false` means 'off'."
  @type binary_color() :: boolean
  @typedoc "An RGB color.  The acceptable range of values depends on the bit depth used."
  @type rgb() :: {non_neg_integer, non_neg_integer, non_neg_integer}
  @typedoc "A BGR color.  The acceptable range of values depends on the bit depth used."
  @type bgr() :: {non_neg_integer, non_neg_integer, non_neg_integer}
  @typedoc "A greyscale color.  The acceptable range of values depends on the bit depth used."
  @type grey() :: non_neg_integer

  require EmbeddedGraphics.Macros

  EmbeddedGraphics.Macros.iffeature "binary_color" do
    @type t() :: binary_color
  end
end
