# SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0 OR MIT

defprotocol EmbeddedGraphics.Eq do
  @moduledoc """
  Compare for equality.

  The `core::cmp::PartialEq` trait in Rust
  """

  @doc "Returns whether or not the two objects are equal"
  @spec eq?(t(), t()) :: boolean()
  def eq?(obj1, obj2)
end
