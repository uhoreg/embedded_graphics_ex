# SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0 OR MIT

defmodule EmbeddedGraphics.Macros do
  defmacro iffeature(feature, do: doblock, else: elseblock) do
    quote do
      if Application.compile_env!(:embedded_graphics, EmbeddedGraphics.Nif)
         |> (&if(Keyword.get(&1, :default_features, true),
               do: ["simulator", "binary_color"],
               else: Keyword.get(&1, :features, [])
             )).()
         |> Enum.member?(unquote(feature)) do
        unquote(doblock)
      else
        unquote(elseblock)
      end
    end
  end

  defmacro iffeature(feature, block) do
    quote do
      if Application.compile_env!(:embedded_graphics, EmbeddedGraphics.Nif)
         |> (&if(Keyword.get(&1, :default_features, true),
               do: ["simulator", "binary_color"],
               else: Keyword.get(&1, :features, [])
             )).()
         |> Enum.member?(unquote(feature)),
         unquote(block)
    end
  end
end
