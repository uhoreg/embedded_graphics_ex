# SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0 OR MIT

defprotocol EmbeddedGraphics.Transform do
  @moduledoc """
  Transformations for graphics objects
  """

  alias EmbeddedGraphics.Geometry

  @doc "Move the origin of an object by a given number of (x, y) pixels, returning a new object"
  @spec translate(object :: t(), by :: Geometry.point()) :: t()
  def translate(object, by)
end
