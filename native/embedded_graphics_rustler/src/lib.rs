// SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
// SPDX-License-Identifier: Apache-2.0 OR MIT

#[macro_use]
extern crate rustler;

use rustler::{Env, NifUnitEnum, Term};

mod draw_target;
mod geometry;
mod pixelcolor;
mod primitives;
mod text;
use crate::{
    draw_target::*,
    primitives::{arc::*, circle::*, ellipse::*, line::*, rectangle::*},
    text::mono_text::*,
};

#[derive(Debug, NifUnitEnum)]
pub(crate) enum Error {
    DisplayError,
    InvalidColor,
    UnknownFont,
}

rustler::init!(
    "Elixir.EmbeddedGraphics.Nif",
    [
        // display/draw_target functions
        new_display,
        display_eq,
        clear_display,
        display_set_allow_overdraw,
        affected_area_of_display,
        display_diff_pattern,
        update_window,
        bounding_box_of_display,
        // primitives functions
        // arc
        new_arc,
        new_arc_with_center,
        new_arc_from_circle,
        arc_to_circle,
        top_left_of_arc,
        diameter_of_arc,
        angle_start_of_arc,
        angle_sweep_of_arc,
        center_of_arc,
        arc_eq,
        bounding_box_of_arc,
        styled_bounding_box_of_arc,
        draw_styled_arc,
        translate_arc,
        // circle
        new_circle,
        new_circle_with_center,
        top_left_of_circle,
        diameter_of_circle,
        center_of_circle,
        circle_eq,
        bounding_box_of_circle,
        circle_contains_point,
        styled_bounding_box_of_circle,
        draw_styled_circle,
        translate_circle,
        // ellipse
        new_ellipse,
        new_ellipse_with_center,
        top_left_of_ellipse,
        size_of_ellipse,
        center_of_ellipse,
        ellipse_eq,
        bounding_box_of_ellipse,
        ellipse_contains_point,
        styled_bounding_box_of_ellipse,
        draw_styled_ellipse,
        translate_ellipse,
        // line
        new_line,
        new_line_with_delta,
        start_of_line,
        end_of_line,
        midpoint_of_line,
        delta_of_line,
        line_eq,
        bounding_box_of_line,
        styled_bounding_box_of_line,
        draw_styled_line,
        translate_line,
        // rectangle
        new_rectangle,
        new_rectangle_with_corners,
        new_rectangle_with_center,
        top_left_of_rectangle,
        size_of_rectangle,
        center_of_rectangle,
        bottom_right_of_rectangle,
        intersection_of_rectangles,
        resized_rectangle,
        resized_width_rectangle,
        resized_height_rectangle,
        offset_rectangle,
        anchor_point_of_rectangle,
        anchor_x_of_rectangle,
        anchor_y_of_rectangle,
        rows_of_rectangle,
        columns_of_rectangle,
        is_rectangle_zero_sized,
        rectangle_eq,
        bounding_box_of_rectangle,
        rectangle_contains_point,
        styled_bounding_box_of_rectangle,
        draw_styled_rectangle,
        translate_rectangle,
        // mono text
        new_mono_text_style,
        new_mono_text,
        bounding_box_of_mono_text,
        draw_mono_text,
        translate_mono_text,
    ],
    load = on_load
);

fn on_load(env: Env, _info: Term) -> bool {
    resource!(DrawTargetResource, env);

    resource!(ArcResource, env);
    resource!(CircleResource, env);
    resource!(EllipseResource, env);
    resource!(LineResource, env);
    resource!(RectangleResource, env);
    resource!(MonoTextStyleResource, env);
    resource!(MonoTextResource, env);
    true
}
