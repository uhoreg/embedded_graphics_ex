// SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
// SPDX-License-Identifier: Apache-2.0 OR MIT

use rustler::{Env, NifUnitEnum, Term};

use embedded_graphics::{geometry, geometry::AngleUnit};

#[derive(Debug, NifUnitEnum)]
pub(crate) enum AnchorPoint {
    TopLeft,
    TopCenter,
    TopRight,
    CenterLeft,
    Center,
    CenterRight,
    BottomLeft,
    BottomCenter,
    BottomRight,
}

impl From<AnchorPoint> for geometry::AnchorPoint {
    fn from(anchor_point: AnchorPoint) -> Self {
        match anchor_point {
            AnchorPoint::TopLeft => Self::TopLeft,
            AnchorPoint::TopCenter => Self::TopCenter,
            AnchorPoint::TopRight => Self::TopRight,
            AnchorPoint::CenterLeft => Self::CenterLeft,
            AnchorPoint::Center => Self::Center,
            AnchorPoint::CenterRight => Self::CenterRight,
            AnchorPoint::BottomLeft => Self::BottomLeft,
            AnchorPoint::BottomCenter => Self::BottomCenter,
            AnchorPoint::BottomRight => Self::BottomRight,
        }
    }
}

#[derive(Debug, NifUnitEnum)]
pub(crate) enum AnchorX {
    Left,
    Center,
    Right,
}

impl From<AnchorX> for geometry::AnchorX {
    fn from(anchor: AnchorX) -> Self {
        match anchor {
            AnchorX::Left => Self::Left,
            AnchorX::Center => Self::Center,
            AnchorX::Right => Self::Right,
        }
    }
}

#[derive(Debug, NifUnitEnum)]
pub(crate) enum AnchorY {
    Top,
    Center,
    Bottom,
}

impl From<AnchorY> for geometry::AnchorY {
    fn from(anchor: AnchorY) -> Self {
        match anchor {
            AnchorY::Top => Self::Top,
            AnchorY::Center => Self::Center,
            AnchorY::Bottom => Self::Bottom,
        }
    }
}

pub(crate) enum Angle {
    Degrees(f32),
    Radians(f32),
}

#[derive(Debug, NifUnitEnum)]
enum AngleType {
    Deg,
    Rad,
}

impl<'a> rustler::types::Decoder<'a> for Angle {
    fn decode(term: Term<'a>) -> rustler::NifResult<Self> {
        let angle: (f32, AngleType) = term.decode()?;
        match angle.1 {
            AngleType::Deg => Ok(Angle::Degrees(angle.0)),
            AngleType::Rad => Ok(Angle::Radians(angle.0)),
        }
    }
}

impl rustler::types::Encoder for Angle {
    fn encode<'a>(&self, env: Env<'a>) -> Term<'a> {
        match self {
            Angle::Degrees(deg) => (deg, AngleType::Deg).encode(env),
            Angle::Radians(rad) => (rad, AngleType::Rad).encode(env),
        }
    }
}

impl From<Angle> for geometry::Angle {
    fn from(angle: Angle) -> Self {
        match angle {
            Angle::Degrees(deg) => deg.deg(),
            Angle::Radians(rad) => rad.rad(),
        }
    }
}

impl From<geometry::Angle> for Angle {
    fn from(angle: geometry::Angle) -> Self {
        // angle is stored internally as radians, so return radians to avoid
        // conversion
        Angle::Radians(angle.to_radians())
    }
}

#[derive(Debug, NifTuple)]
pub(crate) struct Point {
    x: i32,
    y: i32,
}

impl From<Point> for geometry::Point {
    fn from(point: Point) -> Self {
        Self {
            x: point.x,
            y: point.y,
        }
    }
}

impl From<geometry::Point> for Point {
    fn from(point: geometry::Point) -> Self {
        Self {
            x: point.x,
            y: point.y,
        }
    }
}

#[derive(Debug, NifTuple)]
pub(crate) struct Size {
    width: u32,
    height: u32,
}

impl From<Size> for geometry::Size {
    fn from(size: Size) -> Self {
        Self {
            width: size.width,
            height: size.height,
        }
    }
}

impl From<geometry::Size> for Size {
    fn from(size: geometry::Size) -> Self {
        Self {
            width: size.width,
            height: size.height,
        }
    }
}
