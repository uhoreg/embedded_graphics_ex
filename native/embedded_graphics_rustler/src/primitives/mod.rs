// SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
// SPDX-License-Identifier: Apache-2.0 OR MIT

pub(crate) mod arc;
pub(crate) mod circle;
pub(crate) mod ellipse;
pub(crate) mod line;
pub(crate) mod rectangle;

use embedded_graphics::primitives;
use rustler::{NifStruct, NifUnitEnum};

use crate::{pixelcolor::*, Error};

#[derive(Debug, NifUnitEnum)]
enum StrokeAlignment {
    Inside,
    Center,
    Outside,
}

#[derive(Debug, NifStruct)]
#[module = "EmbeddedGraphics.Primitives.PrimitiveStyle"]
struct PrimitiveStyle {
    pub fill_color: Option<ErlColor>,
    pub stroke_color: Option<ErlColor>,
    pub stroke_width: Option<u32>,
    pub stroke_alignment: Option<StrokeAlignment>,
}

fn make_primitive_style(style: PrimitiveStyle) -> Result<primitives::PrimitiveStyle<Color>, Error> {
    let mut builder = primitives::PrimitiveStyleBuilder::new();
    if let Some(color) = style.fill_color {
        builder = builder.fill_color(color.try_into()?);
    }
    if let Some(color) = style.stroke_color {
        builder = builder.stroke_color(color.try_into()?);
    }
    if let Some(width) = style.stroke_width {
        builder = builder.stroke_width(width);
    }
    if let Some(alignment) = style.stroke_alignment {
        let alignment = match alignment {
            StrokeAlignment::Inside => primitives::StrokeAlignment::Inside,
            StrokeAlignment::Center => primitives::StrokeAlignment::Center,
            StrokeAlignment::Outside => primitives::StrokeAlignment::Outside,
        };
        builder = builder.stroke_alignment(alignment);
    }
    Ok(builder.build())
}
