// SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
// SPDX-License-Identifier: Apache-2.0 OR MIT

use embedded_graphics::{
    geometry::Dimensions,
    primitives::{self, StyledDimensions, StyledDrawable},
    transform::Transform,
};
use rustler::resource::ResourceArc;
use std::sync::Mutex;

use super::{circle::CircleResource, make_primitive_style, PrimitiveStyle};
use crate::{
    draw_target::DrawTarget,
    geometry::{Angle, Point},
    primitives::rectangle::RectangleResource,
    Error,
};

#[rustler::nif]
pub(crate) fn new_arc(
    top_left: Point,
    diameter: u32,
    angle_start: Angle,
    angle_sweep: Angle,
) -> ResourceArc<ArcResource> {
    ResourceArc::new(ArcResource(Mutex::new(primitives::Arc::new(
        top_left.into(),
        diameter,
        angle_start.into(),
        angle_sweep.into(),
    ))))
}

#[rustler::nif]
pub(crate) fn new_arc_with_center(
    center: Point,
    diameter: u32,
    angle_start: Angle,
    angle_sweep: Angle,
) -> ResourceArc<ArcResource> {
    ResourceArc::new(ArcResource(Mutex::new(primitives::Arc::with_center(
        center.into(),
        diameter,
        angle_start.into(),
        angle_sweep.into(),
    ))))
}

#[rustler::nif]
pub(crate) fn new_arc_from_circle(
    circle: ResourceArc<CircleResource>,
    angle_start: Angle,
    angle_sweep: Angle,
) -> ResourceArc<ArcResource> {
    let circle = circle.0.lock().unwrap();
    ResourceArc::new(ArcResource(Mutex::new(primitives::Arc::from_circle(
        *circle,
        angle_start.into(),
        angle_sweep.into(),
    ))))
}

#[rustler::nif]
pub(crate) fn arc_to_circle(arc: ResourceArc<ArcResource>) -> ResourceArc<CircleResource> {
    let arc = arc.0.lock().unwrap();
    ResourceArc::new(CircleResource(Mutex::new(arc.to_circle())))
}

#[rustler::nif]
pub(crate) fn top_left_of_arc(arc: ResourceArc<ArcResource>) -> Point {
    arc.0.lock().unwrap().top_left.into()
}

#[rustler::nif]
pub(crate) fn diameter_of_arc(arc: ResourceArc<ArcResource>) -> u32 {
    arc.0.lock().unwrap().diameter
}

#[rustler::nif]
pub(crate) fn angle_start_of_arc(arc: ResourceArc<ArcResource>) -> Angle {
    arc.0.lock().unwrap().angle_start.into()
}

#[rustler::nif]
pub(crate) fn angle_sweep_of_arc(arc: ResourceArc<ArcResource>) -> Angle {
    arc.0.lock().unwrap().angle_sweep.into()
}

#[rustler::nif]
pub(crate) fn center_of_arc(arc: ResourceArc<ArcResource>) -> Point {
    arc.0.lock().unwrap().center().into()
}

// core::cmp::PartialEq trait
#[rustler::nif]
pub(crate) fn arc_eq(a1: ResourceArc<ArcResource>, a2: ResourceArc<ArcResource>) -> bool {
    let a1 = a1.0.lock().unwrap();
    let a2 = a2.0.lock().unwrap();
    *a1 == *a2
}

// embedded_graphics::primitives::Dimensions trait
#[rustler::nif]
pub(crate) fn bounding_box_of_arc(arc: ResourceArc<ArcResource>) -> ResourceArc<RectangleResource> {
    let arc = arc.0.lock().unwrap();
    ResourceArc::new(RectangleResource(Mutex::new(arc.bounding_box())))
}

// embedded_graphics::primitives::StyledDimensions trait
#[rustler::nif]
pub(crate) fn styled_bounding_box_of_arc(
    arc: ResourceArc<ArcResource>,
    style: PrimitiveStyle,
) -> Result<ResourceArc<RectangleResource>, Error> {
    let arc = arc.0.lock().unwrap();
    let style = make_primitive_style(style)?;
    Ok(ResourceArc::new(RectangleResource(Mutex::new(
        arc.styled_bounding_box(&style),
    ))))
}

// embedded_graphics::primitives::StyledDrawable trait
#[rustler::nif]
pub(crate) fn draw_styled_arc(
    arc: ResourceArc<ArcResource>,
    style: PrimitiveStyle,
    target: DrawTarget,
) -> Result<(), Error> {
    let arc = arc.0.lock().unwrap();
    let style = make_primitive_style(style)?;
    arc.draw_styled(&style, &mut target.clone())
        .or(Err(Error::DisplayError))?;
    Ok(())
}

// embedded_graphics::transform::Transform trait
#[rustler::nif]
pub(crate) fn translate_arc(arc: ResourceArc<ArcResource>, by: Point) -> ResourceArc<ArcResource> {
    let arc = arc.0.lock().unwrap();
    ResourceArc::new(ArcResource(Mutex::new(arc.translate(by.into()))))
}

pub(crate) struct ArcResource(Mutex<primitives::Arc>);
