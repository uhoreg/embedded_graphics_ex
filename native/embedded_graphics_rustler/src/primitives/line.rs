// SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
// SPDX-License-Identifier: Apache-2.0 OR MIT

use embedded_graphics::{
    geometry::Dimensions,
    primitives::{self, StyledDimensions, StyledDrawable},
    transform::Transform,
};
use rustler::resource::ResourceArc;
use std::sync::Mutex;

use super::{make_primitive_style, PrimitiveStyle};
use crate::{
    draw_target::DrawTarget, geometry::Point, primitives::rectangle::RectangleResource, Error,
};

#[rustler::nif]
pub(crate) fn new_line(start: Point, end: Point) -> ResourceArc<LineResource> {
    ResourceArc::new(LineResource(Mutex::new(primitives::Line::new(
        start.into(),
        end.into(),
    ))))
}

#[rustler::nif]
pub(crate) fn new_line_with_delta(start: Point, delta: Point) -> ResourceArc<LineResource> {
    ResourceArc::new(LineResource(Mutex::new(primitives::Line::with_delta(
        start.into(),
        delta.into(),
    ))))
}

#[rustler::nif]
pub(crate) fn start_of_line(line: ResourceArc<LineResource>) -> Point {
    line.0.lock().unwrap().start.into()
}

#[rustler::nif]
pub(crate) fn end_of_line(line: ResourceArc<LineResource>) -> Point {
    line.0.lock().unwrap().end.into()
}

#[rustler::nif]
pub(crate) fn midpoint_of_line(line: ResourceArc<LineResource>) -> Point {
    line.0.lock().unwrap().midpoint().into()
}

#[rustler::nif]
pub(crate) fn delta_of_line(line: ResourceArc<LineResource>) -> Point {
    line.0.lock().unwrap().delta().into()
}

// core::cmp::PartialEq trait
#[rustler::nif]
pub(crate) fn line_eq(l1: ResourceArc<LineResource>, l2: ResourceArc<LineResource>) -> bool {
    let l1 = l1.0.lock().unwrap();
    let l2 = l2.0.lock().unwrap();
    *l1 == *l2
}

// embedded_graphics::primitives::Dimensions trait
#[rustler::nif]
pub(crate) fn bounding_box_of_line(
    line: ResourceArc<LineResource>,
) -> ResourceArc<RectangleResource> {
    let line = line.0.lock().unwrap();
    ResourceArc::new(RectangleResource(Mutex::new(line.bounding_box())))
}

// embedded_graphics::primitives::StyledDimensions trait
#[rustler::nif]
pub(crate) fn styled_bounding_box_of_line(
    line: ResourceArc<LineResource>,
    style: PrimitiveStyle,
) -> Result<ResourceArc<RectangleResource>, Error> {
    let line = line.0.lock().unwrap();
    let style = make_primitive_style(style)?;
    Ok(ResourceArc::new(RectangleResource(Mutex::new(
        line.styled_bounding_box(&style),
    ))))
}

// embedded_graphics::primitives::StyledDrawable trait
#[rustler::nif]
pub(crate) fn draw_styled_line(
    line: ResourceArc<LineResource>,
    style: PrimitiveStyle,
    target: DrawTarget,
) -> Result<(), Error> {
    let line = line.0.lock().unwrap();
    let style = make_primitive_style(style)?;
    line.draw_styled(&style, &mut target.clone())
        .or(Err(Error::DisplayError))?;
    Ok(())
}

// embedded_graphics::transform::Transform trait
#[rustler::nif]
pub(crate) fn translate_line(
    line: ResourceArc<LineResource>,
    by: Point,
) -> ResourceArc<LineResource> {
    let line = line.0.lock().unwrap();
    ResourceArc::new(LineResource(Mutex::new(line.translate(by.into()))))
}

pub(crate) struct LineResource(Mutex<primitives::Line>);
