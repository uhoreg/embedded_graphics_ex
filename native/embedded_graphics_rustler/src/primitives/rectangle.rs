// SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
// SPDX-License-Identifier: Apache-2.0 OR MIT

use embedded_graphics::{
    geometry::Dimensions,
    primitives::{self, StyledDimensions, StyledDrawable},
    transform::Transform,
};
use rustler::resource::ResourceArc;
use std::sync::Mutex;

use super::{make_primitive_style, PrimitiveStyle};
use crate::{
    draw_target::DrawTarget,
    geometry::{AnchorPoint, AnchorX, AnchorY, Point, Size},
    Error,
};

#[rustler::nif]
pub(crate) fn new_rectangle(top_left: Point, size: Size) -> ResourceArc<RectangleResource> {
    ResourceArc::new(RectangleResource(Mutex::new(primitives::Rectangle::new(
        top_left.into(),
        size.into(),
    ))))
}

#[rustler::nif]
pub(crate) fn new_rectangle_with_corners(
    corner_1: Point,
    corner_2: Point,
) -> ResourceArc<RectangleResource> {
    ResourceArc::new(RectangleResource(Mutex::new(
        primitives::Rectangle::with_corners(corner_1.into(), corner_2.into()),
    )))
}

#[rustler::nif]
pub(crate) fn new_rectangle_with_center(
    center: Point,
    size: Size,
) -> ResourceArc<RectangleResource> {
    ResourceArc::new(RectangleResource(Mutex::new(
        primitives::Rectangle::with_center(center.into(), size.into()),
    )))
}

#[rustler::nif]
pub(crate) fn top_left_of_rectangle(rectangle: ResourceArc<RectangleResource>) -> Point {
    rectangle.0.lock().unwrap().top_left.into()
}

#[rustler::nif]
pub(crate) fn size_of_rectangle(rectangle: ResourceArc<RectangleResource>) -> Size {
    rectangle.0.lock().unwrap().size.into()
}

#[rustler::nif]
pub(crate) fn center_of_rectangle(rectangle: ResourceArc<RectangleResource>) -> Point {
    rectangle.0.lock().unwrap().center().into()
}

#[rustler::nif]
pub(crate) fn bottom_right_of_rectangle(
    rectangle: ResourceArc<RectangleResource>,
) -> Option<Point> {
    rectangle
        .0
        .lock()
        .unwrap()
        .bottom_right()
        .map(|pt| pt.into())
}

#[rustler::nif]
pub(crate) fn intersection_of_rectangles(
    r1: ResourceArc<RectangleResource>,
    r2: ResourceArc<RectangleResource>,
) -> ResourceArc<RectangleResource> {
    // FIXME: check if r1 and r2 are pointing to the same rectangle
    let r1 = r1.0.lock().unwrap();
    let r2 = r2.0.lock().unwrap();
    ResourceArc::new(RectangleResource(Mutex::new(r1.intersection(&r2))))
}

#[rustler::nif]
pub(crate) fn resized_rectangle(
    rectangle: ResourceArc<RectangleResource>,
    size: Size,
    anchor_point: AnchorPoint,
) -> ResourceArc<RectangleResource> {
    let rectangle = rectangle.0.lock().unwrap();
    ResourceArc::new(RectangleResource(Mutex::new(
        rectangle.resized(size.into(), anchor_point.into()),
    )))
}

#[rustler::nif]
pub(crate) fn resized_width_rectangle(
    rectangle: ResourceArc<RectangleResource>,
    width: u32,
    anchor_x: AnchorX,
) -> ResourceArc<RectangleResource> {
    let rectangle = rectangle.0.lock().unwrap();
    ResourceArc::new(RectangleResource(Mutex::new(
        rectangle.resized_width(width, anchor_x.into()),
    )))
}

#[rustler::nif]
pub(crate) fn resized_height_rectangle(
    rectangle: ResourceArc<RectangleResource>,
    height: u32,
    anchor_y: AnchorY,
) -> ResourceArc<RectangleResource> {
    let rectangle = rectangle.0.lock().unwrap();
    ResourceArc::new(RectangleResource(Mutex::new(
        rectangle.resized_height(height, anchor_y.into()),
    )))
}

#[rustler::nif]
pub(crate) fn offset_rectangle(
    rectangle: ResourceArc<RectangleResource>,
    offset: i32,
) -> ResourceArc<RectangleResource> {
    let rectangle = rectangle.0.lock().unwrap();
    ResourceArc::new(RectangleResource(Mutex::new(rectangle.offset(offset))))
}

#[rustler::nif]
pub(crate) fn anchor_point_of_rectangle(
    rectangle: ResourceArc<RectangleResource>,
    anchor_point: AnchorPoint,
) -> Point {
    rectangle
        .0
        .lock()
        .unwrap()
        .anchor_point(anchor_point.into())
        .into()
}

#[rustler::nif]
pub(crate) fn anchor_x_of_rectangle(
    rectangle: ResourceArc<RectangleResource>,
    anchor_x: AnchorX,
) -> i32 {
    rectangle.0.lock().unwrap().anchor_x(anchor_x.into())
}

#[rustler::nif]
pub(crate) fn anchor_y_of_rectangle(
    rectangle: ResourceArc<RectangleResource>,
    anchor_y: AnchorY,
) -> i32 {
    rectangle.0.lock().unwrap().anchor_y(anchor_y.into())
}

#[rustler::nif]
pub(crate) fn rows_of_rectangle(rectangle: ResourceArc<RectangleResource>) -> (i32, i32) {
    let range = rectangle.0.lock().unwrap().rows();
    // return as a tuple, so that we can convert it to a native Elixir range
    (range.start, range.end)
}

#[rustler::nif]
pub(crate) fn columns_of_rectangle(rectangle: ResourceArc<RectangleResource>) -> (i32, i32) {
    let range = rectangle.0.lock().unwrap().columns();
    // return as a tuple, so that we can convert it to a native Elixir range
    (range.start, range.end)
}

#[rustler::nif]
pub(crate) fn is_rectangle_zero_sized(rectangle: ResourceArc<RectangleResource>) -> bool {
    rectangle.0.lock().unwrap().is_zero_sized()
}

// core::cmp::PartialEq trait
#[rustler::nif]
pub(crate) fn rectangle_eq(
    r1: ResourceArc<RectangleResource>,
    r2: ResourceArc<RectangleResource>,
) -> bool {
    let r1 = r1.0.lock().unwrap();
    let r2 = r2.0.lock().unwrap();
    *r1 == *r2
}

// embedded_graphics::primitives::Dimensions trait
#[rustler::nif]
pub(crate) fn bounding_box_of_rectangle(
    rectangle: ResourceArc<RectangleResource>,
) -> ResourceArc<RectangleResource> {
    let rectangle = rectangle.0.lock().unwrap();
    ResourceArc::new(RectangleResource(Mutex::new(rectangle.bounding_box())))
}

// embedded_graphics::primitives::ContainsPoint trait
#[rustler::nif]
pub(crate) fn rectangle_contains_point(
    rectangle: ResourceArc<RectangleResource>,
    point: Point,
) -> bool {
    rectangle.0.lock().unwrap().contains(point.into())
}

// embedded_graphics::primitives::StyledDimensions trait
#[rustler::nif]
pub(crate) fn styled_bounding_box_of_rectangle(
    rectangle: ResourceArc<RectangleResource>,
    style: PrimitiveStyle,
) -> Result<ResourceArc<RectangleResource>, Error> {
    let rectangle = rectangle.0.lock().unwrap();
    let style = make_primitive_style(style)?;
    Ok(ResourceArc::new(RectangleResource(Mutex::new(
        rectangle.styled_bounding_box(&style),
    ))))
}

// embedded_graphics::primitives::Primitive trait
#[rustler::nif]
pub(crate) fn draw_styled_rectangle(
    rectangle: ResourceArc<RectangleResource>,
    style: PrimitiveStyle,
    target: DrawTarget,
) -> Result<(), Error> {
    let rectangle = rectangle.0.lock().unwrap();
    let style = make_primitive_style(style)?;
    rectangle
        .draw_styled(&style, &mut target.clone())
        .or(Err(Error::DisplayError))?;
    Ok(())
}

// embedded_graphics::transform::Transform trait
#[rustler::nif]
pub(crate) fn translate_rectangle(
    rectangle: ResourceArc<RectangleResource>,
    by: Point,
) -> ResourceArc<RectangleResource> {
    let rectangle = rectangle.0.lock().unwrap();
    ResourceArc::new(RectangleResource(Mutex::new(
        rectangle.translate(by.into()),
    )))
}

pub(crate) struct RectangleResource(pub(crate) Mutex<primitives::Rectangle>);
