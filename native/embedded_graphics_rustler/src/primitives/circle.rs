// SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
// SPDX-License-Identifier: Apache-2.0 OR MIT

use embedded_graphics::{
    geometry::Dimensions,
    primitives::{self, ContainsPoint, StyledDimensions, StyledDrawable},
    transform::Transform,
};
use rustler::resource::ResourceArc;
use std::sync::Mutex;

use super::{make_primitive_style, PrimitiveStyle};
use crate::{
    draw_target::DrawTarget, geometry::Point, primitives::rectangle::RectangleResource, Error,
};

#[rustler::nif]
pub(crate) fn new_circle(top_left: Point, diameter: u32) -> ResourceArc<CircleResource> {
    ResourceArc::new(CircleResource(Mutex::new(primitives::Circle::new(
        top_left.into(),
        diameter,
    ))))
}

#[rustler::nif]
pub(crate) fn new_circle_with_center(center: Point, diameter: u32) -> ResourceArc<CircleResource> {
    ResourceArc::new(CircleResource(Mutex::new(primitives::Circle::with_center(
        center.into(),
        diameter,
    ))))
}

#[rustler::nif]
pub(crate) fn top_left_of_circle(circle: ResourceArc<CircleResource>) -> Point {
    circle.0.lock().unwrap().top_left.into()
}

#[rustler::nif]
pub(crate) fn diameter_of_circle(circle: ResourceArc<CircleResource>) -> u32 {
    circle.0.lock().unwrap().diameter
}

#[rustler::nif]
pub(crate) fn center_of_circle(circle: ResourceArc<CircleResource>) -> Point {
    circle.0.lock().unwrap().center().into()
}

// core::cmp::PartialEq trait
#[rustler::nif]
pub(crate) fn circle_eq(c1: ResourceArc<CircleResource>, c2: ResourceArc<CircleResource>) -> bool {
    let c1 = c1.0.lock().unwrap();
    let c2 = c2.0.lock().unwrap();
    *c1 == *c2
}

// embedded_graphics::geometry::Dimensions trait
#[rustler::nif]
pub(crate) fn bounding_box_of_circle(
    circle: ResourceArc<CircleResource>,
) -> ResourceArc<RectangleResource> {
    let circle = circle.0.lock().unwrap();
    ResourceArc::new(RectangleResource(Mutex::new(circle.bounding_box())))
}

// embedded_graphics::primitives::ContainsPoint trait
#[rustler::nif]
pub(crate) fn circle_contains_point(circle: ResourceArc<CircleResource>, point: Point) -> bool {
    circle.0.lock().unwrap().contains(point.into())
}

// embedded_graphics::primitives::StyledDimensions trait
#[rustler::nif]
pub(crate) fn styled_bounding_box_of_circle(
    circle: ResourceArc<CircleResource>,
    style: PrimitiveStyle,
) -> Result<ResourceArc<RectangleResource>, Error> {
    let circle = circle.0.lock().unwrap();
    let style = make_primitive_style(style)?;
    Ok(ResourceArc::new(RectangleResource(Mutex::new(
        circle.styled_bounding_box(&style),
    ))))
}

// embedded_graphics::primitives::Primitive trait
#[rustler::nif]
pub(crate) fn draw_styled_circle(
    circle: ResourceArc<CircleResource>,
    style: PrimitiveStyle,
    target: DrawTarget,
) -> Result<(), Error> {
    let circle = circle.0.lock().unwrap();
    let style = make_primitive_style(style)?;
    circle
        .draw_styled(&style, &mut target.clone())
        .or(Err(Error::DisplayError))?;
    Ok(())
}

// embedded_graphics::transform::Transform trait
#[rustler::nif]
pub(crate) fn translate_circle(
    circle: ResourceArc<CircleResource>,
    by: Point,
) -> ResourceArc<CircleResource> {
    let circle = circle.0.lock().unwrap();
    ResourceArc::new(CircleResource(Mutex::new(circle.translate(by.into()))))
}

pub(crate) struct CircleResource(pub(crate) Mutex<primitives::Circle>);
