// SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
// SPDX-License-Identifier: Apache-2.0 OR MIT

use embedded_graphics::{
    geometry::Dimensions,
    primitives::{self, ContainsPoint, StyledDimensions, StyledDrawable},
    transform::Transform,
};
use rustler::resource::ResourceArc;
use std::sync::Mutex;

use super::{make_primitive_style, PrimitiveStyle};
use crate::{
    draw_target::DrawTarget,
    geometry::{Point, Size},
    primitives::rectangle::RectangleResource,
    Error,
};

#[rustler::nif]
pub(crate) fn new_ellipse(top_left: Point, size: Size) -> ResourceArc<EllipseResource> {
    ResourceArc::new(EllipseResource(Mutex::new(primitives::Ellipse::new(
        top_left.into(),
        size.into(),
    ))))
}

#[rustler::nif]
pub(crate) fn new_ellipse_with_center(center: Point, size: Size) -> ResourceArc<EllipseResource> {
    ResourceArc::new(EllipseResource(Mutex::new(
        primitives::Ellipse::with_center(center.into(), size.into()),
    )))
}

#[rustler::nif]
pub(crate) fn top_left_of_ellipse(ellipse: ResourceArc<EllipseResource>) -> Point {
    ellipse.0.lock().unwrap().top_left.into()
}

#[rustler::nif]
pub(crate) fn size_of_ellipse(ellipse: ResourceArc<EllipseResource>) -> Size {
    ellipse.0.lock().unwrap().size.into()
}

#[rustler::nif]
pub(crate) fn center_of_ellipse(ellipse: ResourceArc<EllipseResource>) -> Point {
    ellipse.0.lock().unwrap().center().into()
}

// core::cmp::PartialEq trait
#[rustler::nif]
pub(crate) fn ellipse_eq(
    e1: ResourceArc<EllipseResource>,
    e2: ResourceArc<EllipseResource>,
) -> bool {
    let e1 = e1.0.lock().unwrap();
    let e2 = e2.0.lock().unwrap();
    *e1 == *e2
}

// embedded_graphics::geometry::Dimensions trait
#[rustler::nif]
pub(crate) fn bounding_box_of_ellipse(
    ellipse: ResourceArc<EllipseResource>,
) -> ResourceArc<RectangleResource> {
    let ellipse = ellipse.0.lock().unwrap();
    ResourceArc::new(RectangleResource(Mutex::new(ellipse.bounding_box())))
}

// embedded_graphics::primitives::ContainsPoint trait
#[rustler::nif]
pub(crate) fn ellipse_contains_point(ellipse: ResourceArc<EllipseResource>, point: Point) -> bool {
    ellipse.0.lock().unwrap().contains(point.into())
}

// embedded_graphics::primitives::StyledDimensions trait
#[rustler::nif]
pub(crate) fn styled_bounding_box_of_ellipse(
    ellipse: ResourceArc<EllipseResource>,
    style: PrimitiveStyle,
) -> Result<ResourceArc<RectangleResource>, Error> {
    let ellipse = ellipse.0.lock().unwrap();
    let style = make_primitive_style(style)?;
    Ok(ResourceArc::new(RectangleResource(Mutex::new(
        ellipse.styled_bounding_box(&style),
    ))))
}

// embedded_graphics::primitives::Primitive trait
#[rustler::nif]
pub(crate) fn draw_styled_ellipse(
    ellipse: ResourceArc<EllipseResource>,
    style: PrimitiveStyle,
    target: DrawTarget,
) -> Result<(), Error> {
    let ellipse = ellipse.0.lock().unwrap();
    let style = make_primitive_style(style)?;
    ellipse
        .draw_styled(&style, &mut target.clone())
        .or(Err(Error::DisplayError))?;
    Ok(())
}

// embedded_graphics::transform::Transform trait
#[rustler::nif]
pub(crate) fn translate_ellipse(
    ellipse: ResourceArc<EllipseResource>,
    by: Point,
) -> ResourceArc<EllipseResource> {
    let ellipse = ellipse.0.lock().unwrap();
    ResourceArc::new(EllipseResource(Mutex::new(ellipse.translate(by.into()))))
}

pub(crate) struct EllipseResource(pub(crate) Mutex<primitives::Ellipse>);
