// SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
// SPDX-License-Identifier: Apache-2.0 OR MIT

#[cfg(feature = "binary_color")]
mod binary_color;
#[cfg(feature = "binary_color")]
pub(crate) use self::binary_color::*;
