// SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
// SPDX-License-Identifier: Apache-2.0 OR MIT

use embedded_graphics::pixelcolor::BinaryColor;
use rustler::{Env, Term};

use crate::Error;

pub(crate) type Color = BinaryColor;

#[derive(Debug)]
pub(crate) struct ErlColor {
    inner: bool,
}

impl<'a> rustler::types::Decoder<'a> for ErlColor {
    fn decode(term: Term<'a>) -> rustler::NifResult<Self> {
        Ok(ErlColor {
            inner: term.decode()?,
        })
    }
}

impl rustler::types::Encoder for ErlColor {
    fn encode<'a>(&self, env: Env<'a>) -> Term<'a> {
        self.inner.encode(env)
    }
}

impl TryFrom<ErlColor> for Color {
    type Error = Error;
    fn try_from(color: ErlColor) -> Result<Self, Error> {
        if color.inner {
            Ok(Color::On)
        } else {
            Ok(Color::Off)
        }
    }
}
