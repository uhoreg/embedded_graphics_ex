// SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
// SPDX-License-Identifier: Apache-2.0 OR MIT

pub(crate) mod mono_text;

use rustler::NifStruct;

#[derive(NifStruct)]
#[module = "EmbeddedGraphics.Text.TextStyle"]
struct TextStyle {}
