// SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
// SPDX-License-Identifier: Apache-2.0 OR MIT

use std::collections::HashMap;
use std::sync::Arc;
use std::sync::Mutex;

use embedded_graphics::{
    geometry::{Dimensions, Point as RustPoint},
    mono_font::{ascii, MonoFont, MonoTextStyle, MonoTextStyleBuilder},
    text::{DecorationColor, Text, TextStyle, TextStyleBuilder},
    transform::Transform,
    Drawable,
};
use rustler::resource::ResourceArc;
use rustler::NifUnitEnum;

use crate::{
    draw_target::DrawTarget, geometry::Point, pixelcolor::*,
    primitives::rectangle::RectangleResource, Error,
};

#[derive(Debug, Eq, Hash, PartialEq, NifUnitEnum)]
enum FontName {
    Font4x6Ascii,
    Font5x7Ascii,
    Font5x8Ascii,
    Font6x9Ascii,
    Font6x10Ascii,
    Font6x12Ascii,
    Font6x13Ascii,
    Font6x13BoldAscii,
    Font6x13ItalicAscii,
    Font7x13Ascii,
    Font7x13BoldAscii,
    Font7x13ItalicAscii,
    Font7x14Ascii,
    Font7x14BoldAscii,
    Font8x13Ascii,
    Font8x13BoldAscii,
    Font8x13ItalicAscii,
    Font9x15Ascii,
    Font9x15BoldAscii,
    Font9x15ItalicAscii,
    Font9x18Ascii,
    Font9x18BoldAscii,
    Font9x18ItalicAscii,
    Font10x10Ascii,
}

rustler::lazy_static::lazy_static! {
    static ref FONT_MAP: HashMap<FontName, &'static MonoFont<'static>> = HashMap::from([
        (FontName::Font4x6Ascii, &ascii::FONT_4X6),
        (FontName::Font5x7Ascii, &ascii::FONT_5X7),
        (FontName::Font5x8Ascii, &ascii::FONT_5X8),
        (FontName::Font6x9Ascii, &ascii::FONT_6X9),
        (FontName::Font6x10Ascii, &ascii::FONT_6X10),
        (FontName::Font6x12Ascii, &ascii::FONT_6X12),
        (FontName::Font6x13Ascii, &ascii::FONT_6X13),
        (FontName::Font6x13BoldAscii, &ascii::FONT_6X13_BOLD),
        (FontName::Font6x13ItalicAscii, &ascii::FONT_6X13_ITALIC),
        (FontName::Font7x13Ascii, &ascii::FONT_7X13),
        (FontName::Font7x13BoldAscii, &ascii::FONT_7X13_BOLD),
        (FontName::Font7x13ItalicAscii, &ascii::FONT_6X13_ITALIC),
        (FontName::Font7x14Ascii, &ascii::FONT_7X14),
        (FontName::Font7x14BoldAscii, &ascii::FONT_7X14_BOLD),
        (FontName::Font8x13Ascii, &ascii::FONT_8X13),
        (FontName::Font8x13BoldAscii, &ascii::FONT_8X13_BOLD),
        (FontName::Font8x13ItalicAscii, &ascii::FONT_8X13_ITALIC),
        (FontName::Font9x15Ascii, &ascii::FONT_9X15),
        (FontName::Font9x15BoldAscii, &ascii::FONT_9X15_BOLD),
        (FontName::Font9x18Ascii, &ascii::FONT_9X18),
        (FontName::Font9x18BoldAscii, &ascii::FONT_9X18_BOLD),
        (FontName::Font10x10Ascii, &ascii::FONT_10X20),
    ]);
}

struct ErlMonoTextStyle {
    text_color: Option<Color>,
    background_color: Option<Color>,
    underline_color: DecorationColor<Color>,
    strikethrough_color: DecorationColor<Color>,
    font: &'static MonoFont<'static>,
}

struct MonoText {
    text: Arc<String>,
    style: ResourceArc<MonoTextStyleResource>,
    position: RustPoint,
    text_style: Arc<TextStyle>,
}

#[rustler::nif]
pub(crate) fn new_mono_text_style(
    color: ErlColor,
    font: FontName,
) -> Result<ResourceArc<MonoTextStyleResource>, Error> {
    Ok(ResourceArc::new(MonoTextStyleResource(Mutex::new(
        ErlMonoTextStyle {
            text_color: Some(color.try_into()?),
            background_color: None,
            underline_color: DecorationColor::None,
            strikethrough_color: DecorationColor::None,
            font: FONT_MAP.get(&font).ok_or(Error::UnknownFont)?,
        },
    ))))
}

#[rustler::nif]
// TODO: add font
pub(crate) fn new_mono_text(
    text: &str,
    position: Point,
    style: ResourceArc<MonoTextStyleResource>,
) -> ResourceArc<MonoTextResource> {
    ResourceArc::new(MonoTextResource(Mutex::new(MonoText {
        text: Arc::new(text.to_string()),
        style,
        position: position.into(),
        text_style: Arc::new(TextStyleBuilder::new().build()),
    })))
}

// due to lifetimes, we need to make a new Text object for every function call
// we make from BEAM
fn make_text(text_info: &MonoText) -> Text<MonoTextStyle<Color>> {
    let style = text_info.style.0.lock().unwrap();
    let mut style_builder = MonoTextStyleBuilder::new();
    style_builder = style_builder.font(style.font);
    if let Some(color) = style.text_color {
        style_builder = style_builder.text_color(color)
    }
    if let Some(color) = style.background_color {
        style_builder = style_builder.background_color(color)
    }
    match style.underline_color {
        DecorationColor::None => (),
        DecorationColor::TextColor => style_builder = style_builder.underline(),
        DecorationColor::Custom(color) => style_builder = style_builder.underline_with_color(color),
    }
    match style.strikethrough_color {
        DecorationColor::None => (),
        DecorationColor::TextColor => style_builder = style_builder.strikethrough(),
        DecorationColor::Custom(color) => {
            style_builder = style_builder.strikethrough_with_color(color)
        }
    }
    let style = style_builder.build();
    Text::with_text_style(
        &text_info.text,
        text_info.position,
        style,
        *text_info.text_style,
    )
}

// embedded_graphics::geometry::Dimensions trait
#[rustler::nif]
pub(crate) fn bounding_box_of_mono_text(
    text_info: ResourceArc<MonoTextResource>,
) -> ResourceArc<RectangleResource> {
    let text_info = text_info.0.lock().unwrap();
    let text = make_text(&text_info);
    ResourceArc::new(RectangleResource(Mutex::new(text.bounding_box())))
}

// embedded_graphics::Drawable trait
#[rustler::nif]
pub(crate) fn draw_mono_text(
    text_info: ResourceArc<MonoTextResource>,
    target: DrawTarget,
) -> Result<(), Error> {
    let text_info = text_info.0.lock().unwrap();
    let text = make_text(&text_info);
    text.draw(&mut target.clone())
        .or(Err(Error::DisplayError))?;
    Ok(())
}

// embedded_graphics::transform::Transform trait
#[rustler::nif]
pub(crate) fn translate_mono_text(
    text_info: ResourceArc<MonoTextResource>,
    by: Point,
) -> ResourceArc<MonoTextResource> {
    let text_info = text_info.0.lock().unwrap();
    let mut text = make_text(&text_info);
    text = text.translate(by.into());
    ResourceArc::new(MonoTextResource(Mutex::new(MonoText {
        text: Arc::clone(&text_info.text),
        style: ResourceArc::clone(&text_info.style),
        position: text.position,
        text_style: Arc::clone(&text_info.text_style),
    })))
}

pub(crate) struct MonoTextStyleResource(Mutex<ErlMonoTextStyle>);
pub(crate) struct MonoTextResource(Mutex<MonoText>);
