// SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
// SPDX-License-Identifier: Apache-2.0 OR MIT

use rustler::{resource::ResourceArc, NifStruct};
use std::sync::Mutex;

use embedded_graphics::{
    draw_target::DrawTarget as DrawTargetTrait,
    geometry::{Dimensions, Point},
    mock_display::MockDisplay,
    pixelcolor::{Rgb888, RgbColor},
    primitives::rectangle::Rectangle,
    Pixel,
};

use crate::{pixelcolor::*, primitives::rectangle::RectangleResource, Error};

pub(crate) struct DrawTargetResource(Mutex<MockDisplay<Color>>);

#[derive(Clone, NifStruct)]
#[module = "EmbeddedGraphics.DrawTarget.MockDisplay"]
pub(crate) struct MockDisplayEx {
    inner: ResourceArc<DrawTargetResource>,
}

pub(crate) type DrawTarget = MockDisplayEx;

impl DrawTargetTrait for MockDisplayEx {
    type Color = Color;
    type Error = Error;

    fn draw_iter<I>(&mut self, pixels: I) -> Result<(), Self::Error>
    where
        I: IntoIterator<Item = Pixel<Self::Color>>,
    {
        let mut target = self.inner.0.lock().unwrap();
        target.draw_iter(pixels).or(Err(Error::DisplayError))
    }

    fn fill_contiguous<I>(&mut self, area: &Rectangle, colors: I) -> Result<(), Self::Error>
    where
        I: IntoIterator<Item = Self::Color>,
    {
        let mut target = self.inner.0.lock().unwrap();
        target
            .fill_contiguous(area, colors)
            .or(Err(Error::DisplayError))
    }

    fn fill_solid(&mut self, area: &Rectangle, color: Self::Color) -> Result<(), Self::Error> {
        let mut target = self.inner.0.lock().unwrap();
        target.fill_solid(area, color).or(Err(Error::DisplayError))
    }

    fn clear(&mut self, color: Self::Color) -> Result<(), Self::Error> {
        let mut target = self.inner.0.lock().unwrap();
        target.clear(color).or(Err(Error::DisplayError))
    }
}

impl Dimensions for MockDisplayEx {
    fn bounding_box(&self) -> Rectangle {
        let target = self.inner.0.lock().unwrap();
        target.bounding_box()
    }
}

#[rustler::nif]
pub(crate) fn new_display() -> MockDisplayEx {
    MockDisplayEx {
        inner: ResourceArc::new(DrawTargetResource(Mutex::new(MockDisplay::new()))),
    }
}

#[rustler::nif]
pub(crate) fn display_set_allow_overdraw(target: MockDisplayEx, value: bool) -> () {
    target
        .clone()
        .inner
        .0
        .lock()
        .unwrap()
        .set_allow_overdraw(value)
}

#[rustler::nif]
pub(crate) fn display_diff_pattern(
    target: MockDisplayEx,
    pattern: Vec<&str>,
) -> Option<Vec<String>> {
    let expected_display = MockDisplay::from_pattern(&pattern);
    let target = target.inner.0.lock().unwrap();
    if *target == expected_display {
        None
    } else {
        let diff = target.diff(&expected_display);
        let affected_area = target.affected_area();
        let width = affected_area.top_left.x + (affected_area.size.width as i32);
        let height = affected_area.top_left.y + (affected_area.size.height as i32);
        let mut diff_pixels: Vec<String> = Vec::new();
        for y in 0..height {
            diff_pixels.push(
                (0..width)
                    .map(|x| match diff.get_pixel(Point::new(x, y)) {
                        None => " ",
                        Some(Rgb888::GREEN) => "+",
                        Some(Rgb888::RED) => "-",
                        Some(Rgb888::BLUE) => "X",
                        _ => "?",
                    })
                    .collect(),
            )
        }
        Some(diff_pixels)
    }
}

#[rustler::nif]
pub(crate) fn affected_area_of_display(target: MockDisplayEx) -> ResourceArc<RectangleResource> {
    let target = target.inner.0.lock().unwrap();
    ResourceArc::new(RectangleResource(Mutex::new(target.affected_area())))
}

// core::cmp::PartialEq trait
#[rustler::nif]
pub(crate) fn display_eq(t1: MockDisplayEx, t2: MockDisplayEx) -> bool {
    let t1 = t1.inner.0.lock().unwrap();
    let t2 = t2.inner.0.lock().unwrap();
    *t1 == *t2
}

// embedded_graphics::draw_target::DrawTarget trait
#[rustler::nif]
pub(crate) fn clear_display(target: MockDisplayEx, color: ErlColor) -> Result<(), Error> {
    target.clone().clear(color.try_into()?)
}

#[rustler::nif]
pub(crate) fn bounding_box_of_display(target: MockDisplayEx) -> ResourceArc<RectangleResource> {
    ResourceArc::new(RectangleResource(Mutex::new(target.bounding_box())))
}

// stub functions
#[rustler::nif]
pub(crate) fn update_window() {}
