// SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
// SPDX-License-Identifier: Apache-2.0 OR MIT

#[cfg(feature = "mock")]
mod mock;
#[cfg(feature = "mock")]
pub(crate) use self::mock::*;

#[cfg(feature = "simulator")]
mod simulator;
#[cfg(feature = "simulator")]
pub(crate) use self::simulator::*;
