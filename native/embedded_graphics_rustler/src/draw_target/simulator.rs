// SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
// SPDX-License-Identifier: Apache-2.0 OR MIT

use rustler::{resource::ResourceArc, NifStruct};
use std::collections::HashMap;
use std::sync::Mutex;

use embedded_graphics::{
    draw_target::DrawTarget as DrawTargetTrait,
    geometry::{Dimensions, Size},
    primitives::rectangle::Rectangle,
    Pixel,
};
use embedded_graphics_simulator::{
    // BinaryColorTheme,
    OutputSettingsBuilder,
    SimulatorDisplay,
    Window,
};

use crate::{pixelcolor::*, primitives::rectangle::RectangleResource, Error};

struct WindowStorage {
    windows: HashMap<u32, Window>,
    next_id: u32,
}

unsafe impl Send for WindowStorage {}

rustler::lazy_static::lazy_static! {
    static ref WINDOWS: Mutex<WindowStorage> = Mutex::new(WindowStorage {
        windows: HashMap::new(),
        next_id: 0,
    });
}

pub(crate) struct InnerDrawTarget {
    display: SimulatorDisplay<Color>,
    window_id: u32,
}

pub(crate) struct DrawTargetResource(Mutex<InnerDrawTarget>);

#[derive(Clone, NifStruct)]
#[module = "EmbeddedGraphics.DrawTarget.SimulatorDisplay"]
pub(crate) struct SimulatorDisplayEx {
    inner: ResourceArc<DrawTargetResource>,
}

pub(crate) type DrawTarget = SimulatorDisplayEx;

impl DrawTargetTrait for SimulatorDisplayEx {
    type Color = Color;
    type Error = Error;

    fn draw_iter<I>(&mut self, pixels: I) -> Result<(), Self::Error>
    where
        I: IntoIterator<Item = Pixel<Self::Color>>,
    {
        let target = &mut self.inner.0.lock().unwrap().display;
        target.draw_iter(pixels).or(Err(Error::DisplayError))
    }

    fn fill_contiguous<I>(&mut self, area: &Rectangle, colors: I) -> Result<(), Self::Error>
    where
        I: IntoIterator<Item = Self::Color>,
    {
        let target = &mut self.inner.0.lock().unwrap().display;
        target
            .fill_contiguous(area, colors)
            .or(Err(Error::DisplayError))
    }

    fn fill_solid(&mut self, area: &Rectangle, color: Self::Color) -> Result<(), Self::Error> {
        let target = &mut self.inner.0.lock().unwrap().display;
        target.fill_solid(area, color).or(Err(Error::DisplayError))
    }

    fn clear(&mut self, color: Self::Color) -> Result<(), Self::Error> {
        let target = &mut self.inner.0.lock().unwrap().display;
        target.clear(color).or(Err(Error::DisplayError))
    }
}

impl Dimensions for SimulatorDisplayEx {
    fn bounding_box(&self) -> Rectangle {
        let target = &self.inner.0.lock().unwrap().display;
        target.bounding_box()
    }
}

#[rustler::nif]
pub(crate) fn new_display(size: (u32, u32), window_name: String) -> SimulatorDisplayEx {
    let display = SimulatorDisplay::<Color>::new(Size::new(size.0, size.1));
    let mut windows = WINDOWS.lock().unwrap();
    let output_settings = OutputSettingsBuilder::new()
        //.theme(BinaryColorTheme::OledBlue)
        .build();
    let window = Window::new(&window_name, &output_settings);
    let id = windows.next_id;
    windows.windows.insert(id, window);
    windows.next_id = id + 1;
    SimulatorDisplayEx {
        inner: ResourceArc::new(DrawTargetResource(Mutex::new(InnerDrawTarget {
            display,
            window_id: id,
        }))),
    }
}

#[rustler::nif]
pub(crate) fn update_window(target: SimulatorDisplayEx) {
    let target = target.inner.0.lock().unwrap();
    let mut windows = WINDOWS.lock().unwrap();
    let window = windows.windows.get_mut(&target.window_id).unwrap();
    window.update(&target.display);
}

// embedded_graphics::draw_target::DrawTarget trait
#[rustler::nif]
pub(crate) fn clear_display(target: SimulatorDisplayEx, color: ErlColor) -> Result<(), Error> {
    target.clone().clear(color.try_into()?)
}

// embedded_graphics::geometry::Dimensions trait
#[rustler::nif]
pub(crate) fn bounding_box_of_display(
    target: SimulatorDisplayEx,
) -> ResourceArc<RectangleResource> {
    ResourceArc::new(RectangleResource(Mutex::new(target.bounding_box())))
}

// stub functions
#[rustler::nif]
pub(crate) fn affected_area_of_display() {}
#[rustler::nif]
pub(crate) fn display_eq() {}
#[rustler::nif]
pub(crate) fn display_diff_pattern() {}
#[rustler::nif]
pub(crate) fn display_set_allow_overdraw() {}
