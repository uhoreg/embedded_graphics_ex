# SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0 OR MIT

# This is a port of the tests in
# https://docs.rs/embedded-graphics/0.8.1/src/embedded_graphics/primitives/arc/mod.rs.html and
# https://docs.rs/embedded-graphics/0.8.1/src/embedded_graphics/primitives/arc/styled.rs.html,
# which are Copyright James Waples

defmodule EmbeddedGraphics.Primitives.ArcTest do
  use ExUnit.Case
  doctest EmbeddedGraphics.Primitives.Arc

  alias EmbeddedGraphics.Eq
  alias EmbeddedGraphics.Drawable
  alias EmbeddedGraphics.DrawTarget.MockDisplay
  alias EmbeddedGraphics.Geometry.Dimensions
  alias EmbeddedGraphics.Primitives.Arc
  alias EmbeddedGraphics.Primitives.PrimitiveStyle
  alias EmbeddedGraphics.Primitives.Rectangle
  alias EmbeddedGraphics.Primitives.Styled
  alias EmbeddedGraphics.Transform

  test "negative dimensions" do
    arc = Arc.new({-15, -15}, 10, {0.0, :deg}, {90.0, :deg})

    assert Eq.eq?(
             Dimensions.bounding_box(arc),
             Rectangle.new({-15, -15}, {10, 10})
           )
  end

  test "dimensions" do
    arc = Arc.new({5, 15}, 10, {0.0, :deg}, {90.0, :deg})

    assert Eq.eq?(
             Dimensions.bounding_box(arc),
             Rectangle.new({5, 15}, {10, 10})
           )
  end

  test "center is correct" do
    # odd diameter
    arc = Arc.new({10, 10}, 5, {0.0, :deg}, {90.0, :deg})
    assert Arc.center(arc) == {12, 12}

    # even diameter
    arc = Arc.new({10, 10}, 6, {0.0, :deg}, {90.0, :deg})
    assert Arc.center(arc) == {12, 12}

    # odd diameter
    arc = Arc.new_with_center({10, 10}, 5, {0.0, :deg}, {90.0, :deg})
    assert Arc.center(arc) == {10, 10}

    # even diameter
    arc = Arc.new_with_center({10, 10}, 6, {0.0, :deg}, {90.0, :deg})
    assert Arc.center(arc) == {10, 10}
  end

  test "transform" do
    arc = Arc.new({5, 10}, 10, {0.0, :deg}, {90.0, :deg})
    moved = Transform.translate(arc, {10, 10})

    assert Arc.top_left(moved) == {15, 20}
  end

  test "tiny arc" do
    display = MockDisplay.new()

    Arc.new({0, 0}, 7, {210.0, :deg}, {120.0, :deg})
    |> Styled.new(%PrimitiveStyle{stroke_color: true, stroke_width: 1})
    |> Drawable.draw(display)

    assert MockDisplay.diff_pattern(display, ["  ###  ", " #   # "]) == nil
  end

  @tag skip: "this test needs the DrawTargetExt trait implemented"
  test "quadrant arcs" do
    # TODO: ...
  end

  test "stroke alignment" do
    center = {15, 15}
    size = 10

    style = %PrimitiveStyle{stroke_color: true, stroke_width: 3}

    display_center = MockDisplay.new()

    Arc.new_with_center(center, size, {0.0, :deg}, {90.0, :deg})
    |> Styled.new(style)
    |> Drawable.draw(display_center)

    display_inside = MockDisplay.new()

    Arc.new_with_center(center, size + 2, {0.0, :deg}, {90.0, :deg})
    |> Styled.new(%{style | stroke_alignment: :inside})
    |> Drawable.draw(display_inside)

    display_outside = MockDisplay.new()

    Arc.new_with_center(center, size - 4, {0.0, :deg}, {90.0, :deg})
    |> Styled.new(%{style | stroke_alignment: :outside})
    |> Drawable.draw(display_outside)

    assert Eq.eq?(display_inside, display_center)
    assert Eq.eq?(display_outside, display_center)
  end

  test "bounding boxes" do
    center = {15, 15}
    size = 10

    arc = Arc.new_with_center(center, size, {0.0, :deg}, {90.0, :deg})

    transparent_arc = Styled.new(arc, %PrimitiveStyle{stroke_width: 5})
    stroked_arc = Styled.new(arc, %PrimitiveStyle{stroke_width: 5, stroke_color: true})

    assert Eq.eq?(Dimensions.bounding_box(transparent_arc), Dimensions.bounding_box(stroked_arc))
  end
end
