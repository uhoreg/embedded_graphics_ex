# SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0 OR MIT

# This is a port of the tests in
# https://docs.rs/embedded-graphics/0.8.1/src/embedded_graphics/primitives/rectangle/mod.rs.html and
# https://docs.rs/embedded-graphics/0.8.1/src/embedded_graphics/primitives/rectangle/styled.rs.html,
# which are Copyright James Waples

defmodule EmbeddedGraphics.Primitives.RectangleTest do
  use ExUnit.Case
  doctest EmbeddedGraphics.Primitives.Rectangle

  alias EmbeddedGraphics.Eq
  alias EmbeddedGraphics.Drawable
  alias EmbeddedGraphics.DrawTarget.MockDisplay
  alias EmbeddedGraphics.Geometry.Dimensions
  alias EmbeddedGraphics.Primitives.PrimitiveStyle
  alias EmbeddedGraphics.Primitives.Rectangle
  alias EmbeddedGraphics.Primitives.Styled
  alias EmbeddedGraphics.Transform

  test "dimensions" do
    rect = Rectangle.new({5, 10}, {10, 20})

    assert Eq.eq?(
             Dimensions.bounding_box(rect),
             Rectangle.new({5, 10}, {10, 20})
           )
  end

  test "center" do
    odd = Rectangle.new({10, 20}, {5, 7})
    assert Rectangle.center(odd) == {12, 23}

    even = Rectangle.new({20, 30}, {4, 8})
    assert Rectangle.center(even) == {21, 33}
  end

  test "bottom right" do
    zero = Rectangle.new({10, 20}, {0, 0})
    assert Rectangle.bottom_right(zero) == nil

    odd = Rectangle.new({10, 20}, {5, 7})
    assert Rectangle.bottom_right(odd) == {14, 26}

    even = Rectangle.new({20, 30}, {4, 8})
    assert Rectangle.bottom_right(even) == {23, 37}
  end

  test "rectangle intersection" do
    rect1 = Rectangle.new({10, 10}, {20, 30})
    rect2 = Rectangle.new({25, 25}, {30, 40})

    assert Eq.eq?(
             Rectangle.intersection(rect1, rect2),
             Rectangle.new({25, 25}, {5, 15})
           )
  end

  test "rectangle no intersection" do
    rect1 = Rectangle.new({10, 10}, {20, 30})
    rect2 = Rectangle.new({35, 35}, {30, 40})

    assert Eq.eq?(
             Rectangle.intersection(rect1, rect2),
             Rectangle.new({0, 0}, {0, 0})
           )
  end

  test "rectangle complete intersection" do
    rect1 = Rectangle.new({10, 10}, {20, 30})
    # we need to create a new one, otherwise when we call Rectangle.intersection,
    # the Rust code will try to lock the same rectangle twice, and it will
    # deadlock itself
    # FIXME: we should create a "clone" function?
    rect2 = Rectangle.new({10, 10}, {20, 30})

    assert Eq.eq?(Rectangle.intersection(rect1, rect2), rect1)
  end

  test "rectangle contained intersection" do
    rect1 = Rectangle.new_with_corners({10, 10}, {20, 30})
    rect2 = Rectangle.new_with_corners({5, 5}, {30, 40})

    assert Eq.eq?(Rectangle.intersection(rect1, rect2), rect1)
  end

  test "zero sized intersection" do
    rect1 = Rectangle.new({1, 2}, {0, 0})
    rect2 = Rectangle.new({-10, -10}, {20, 20})

    assert Eq.eq?(Rectangle.intersection(rect1, rect2), rect1)

    rect1 = Rectangle.new({-10, -10}, {20, 20})
    rect2 = Rectangle.new({2, 3}, {0, 0})

    assert Eq.eq?(Rectangle.intersection(rect1, rect2), rect2)
  end

  test "issue 452 broken intersection check" do
    rect1 = Rectangle.new({50, 0}, {75, 200})
    rect2 = Rectangle.new({0, 75}, {200, 50})

    expected = Rectangle.new({50, 75}, {75, 50})

    assert Eq.eq?(Rectangle.intersection(rect1, rect2), expected)
    assert Eq.eq?(Rectangle.intersection(rect2, rect1), expected)
  end

  test "offset" do
    center = {10, 20}
    rect = Rectangle.new_with_center(center, {3, 4})

    assert Eq.eq?(Rectangle.offset(rect, 0), rect)

    assert Eq.eq?(
             Rectangle.offset(rect, 1),
             Rectangle.new_with_center(center, {5, 6})
           )

    assert Eq.eq?(
             Rectangle.offset(rect, 2),
             Rectangle.new_with_center(center, {7, 8})
           )

    assert Eq.eq?(
             Rectangle.offset(rect, -1),
             Rectangle.new_with_center(center, {1, 2})
           )

    assert Eq.eq?(
             Rectangle.offset(rect, -2),
             Rectangle.new_with_center(center, {0, 0})
           )

    assert Eq.eq?(
             Rectangle.offset(rect, -3),
             Rectangle.new_with_center(center, {0, 0})
           )
  end

  def test_resized(rect, target_size, tests) do
    import EmbeddedGraphics.Geometry,
      only: [
        anchor_point_x: 1,
        anchor_point_y: 1,
        point_x: 1,
        point_y: 1,
        size_width: 1,
        size_height: 1
      ]

    Enum.each(tests, fn {anchor_point, expected_top_left} ->
      resized = Rectangle.resized(rect, target_size, anchor_point)
      expected = Rectangle.new(expected_top_left, target_size)

      assert Eq.eq?(resized, expected), inspect(anchor_point)

      resized_x =
        Rectangle.resized_width(rect, size_width(target_size), anchor_point_x(anchor_point))

      assert Rectangle.top_left(resized_x) ==
               {Rectangle.top_left(resized) |> point_x(), Rectangle.top_left(rect) |> point_y()}

      assert Rectangle.size(resized_x) ==
               {Rectangle.size(resized) |> size_width(), Rectangle.size(rect) |> size_height()}

      resized_y =
        Rectangle.resized_height(rect, size_height(target_size), anchor_point_y(anchor_point))

      assert Rectangle.top_left(resized_y) ==
               {Rectangle.top_left(rect) |> point_x(), Rectangle.top_left(resized) |> point_y()}

      assert Rectangle.size(resized_y) ==
               {Rectangle.size(rect) |> size_width(), Rectangle.size(resized) |> size_height()}
    end)
  end

  test "resized smaller" do
    test_resized(
      Rectangle.new({10, 20}, {30, 40}),
      {10, 20},
      [
        {:top_left, {10, 20}},
        {:top_center, {20, 20}},
        {:top_right, {30, 20}},
        {:center_left, {10, 30}},
        {:center, {20, 30}},
        {:center_right, {30, 30}},
        {:bottom_left, {10, 40}},
        {:bottom_center, {20, 40}},
        {:bottom_right, {30, 40}}
      ]
    )
  end

  test "resized larger" do
    test_resized(
      Rectangle.new({10, 20}, {30, 40}),
      {40, 50},
      [
        {:top_left, {10, 20}},
        {:top_center, {5, 20}},
        {:top_right, {0, 20}},
        {:center_left, {10, 15}},
        {:center, {5, 15}},
        {:center_right, {0, 15}},
        {:bottom_left, {10, 10}},
        {:bottom_center, {5, 10}},
        {:bottom_right, {0, 10}}
      ]
    )
  end

  test "resized to zero size" do
    test_resized(
      Rectangle.new({10, 20}, {21, 31}),
      {0, 0},
      [
        {:top_left, {10, 20}},
        {:top_center, {20, 20}},
        {:top_right, {30, 20}},
        {:center_left, {10, 35}},
        {:center, {20, 35}},
        {:center_right, {30, 35}},
        {:bottom_left, {10, 50}},
        {:bottom_center, {20, 50}},
        {:bottom_right, {30, 50}}
      ]
    )
  end

  test "anchor point" do
    import EmbeddedGraphics.Geometry,
      only: [anchor_point_x: 1, anchor_point_y: 1, point_x: 1, point_y: 1]

    rect = Rectangle.new({10, 20}, {21, 31})

    Enum.each(
      [
        {:top_left, {10, 20}},
        {:top_center, {20, 20}},
        {:top_right, {30, 20}},
        {:center_left, {10, 35}},
        {:center, {20, 35}},
        {:center_right, {30, 35}},
        {:bottom_left, {10, 50}},
        {:bottom_center, {20, 50}},
        {:bottom_right, {30, 50}}
      ],
      fn {anchor_point, expected} ->
        assert Rectangle.anchor_point(rect, anchor_point) == expected,
               "#{anchor_point}"

        assert Rectangle.anchor_x(rect, anchor_point_x(anchor_point)) == point_x(expected),
               "#{anchor_point}.x"

        assert Rectangle.anchor_y(rect, anchor_point_y(anchor_point)) == point_y(expected),
               "#{anchor_point}.y"
      end
    )
  end

  test "rows and columns zero sized" do
    rect = Rectangle.new({0, 0}, {0, 0})

    assert Enum.count(Rectangle.rows(rect)) == 0,
           "the rows iterator for a zero sized rectangle shouldn't return any items"

    assert Enum.count(Rectangle.columns(rect)) == 0,
           "the columns iterator for a zero sized rectangle shouldn't return any items"
  end
end
