# SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0 OR MIT

# This is a port of the tests in
# https://docs.rs/embedded-graphics/0.8.1/src/embedded_graphics/primitives/ellipse/mod.rs.html and
# https://docs.rs/embedded-graphics/0.8.1/src/embedded_graphics/primitives/ellipse/styled.rs.html,
# which are Copyright James Waples

defmodule EmbeddedGraphics.Primitives.EllipseTest do
  use ExUnit.Case
  doctest EmbeddedGraphics.Primitives.Ellipse

  alias EmbeddedGraphics.Eq
  alias EmbeddedGraphics.Drawable
  alias EmbeddedGraphics.DrawTarget.MockDisplay
  alias EmbeddedGraphics.Geometry.Dimensions
  alias EmbeddedGraphics.Primitives.Circle
  alias EmbeddedGraphics.Primitives.Ellipse
  alias EmbeddedGraphics.Primitives.PrimitiveStyle
  alias EmbeddedGraphics.Primitives.Rectangle
  alias EmbeddedGraphics.Primitives.Styled
  alias EmbeddedGraphics.Transform

  @tag skip: "this test needs points iterators"
  test "contains" do
    # TODO: ...
  end

  test "translate" do
    moved =
      Ellipse.new({4, 6}, {5, 8})
      |> Transform.translate({3, 5})

    assert Eq.eq?(moved, Ellipse.new({7, 11}, {5, 8}))
  end

  @tag skip: "offset isn't implemented yet"
  test "offset" do
    # TODO: ...
  end

  def test_circles(style = %PrimitiveStyle{}) do
    Enum.each(0..40, fn diameter ->
      stroke_width =
        case style.stroke_width do
          nil -> 0
          _ -> style.stroke_width
        end

      top_left = {stroke_width, stroke_width}

      expected = MockDisplay.new()

      Circle.new(top_left, diameter)
      |> Styled.new(style)
      |> Drawable.draw(expected)

      display = MockDisplay.new()

      Ellipse.new(top_left, {diameter, diameter})
      |> Styled.new(style)
      |> Drawable.draw(display)

      assert Eq.eq?(display, expected), "diameter = #{diameter}"
    end)
  end

  def test_ellipse(size, style = %PrimitiveStyle{}, pattern) do
    ellipse = Ellipse.new({0, 0}, size) |> Styled.new(style)

    drawable = MockDisplay.new()
    Drawable.draw(ellipse, drawable)
    assert MockDisplay.diff_pattern(drawable, pattern) == nil

    # TODO: we need pixel iterators for this
    # let mut pixels = MockDisplayDisplay::new();
    # ellipse.pixels().draw(&mut pixels).unwrap();
    # pixels.assert_pattern(pattern);
  end

  test "ellipse equals circle fill" do
    test_circles(%PrimitiveStyle{fill_color: true})
  end

  test "ellipse equals circle stroke 1px" do
    test_circles(%PrimitiveStyle{stroke_color: true, stroke_width: 1})
  end

  test "ellipse equals circle stroke 10px" do
    test_circles(%PrimitiveStyle{stroke_color: true, stroke_width: 10})
  end

  test "filled ellipse" do
    test_ellipse({20, 10}, %PrimitiveStyle{fill_color: true}, [
      "      ########      ",
      "   ##############   ",
      " ################## ",
      "####################",
      "####################",
      "####################",
      "####################",
      " ################## ",
      "   ##############   ",
      "      ########      "
    ])
  end

  test "thick_stroke_glitch" do
    test_ellipse(
      {11, 21},
      %PrimitiveStyle{
        stroke_width: 10,
        stroke_color: true,
        stroke_alignment: :inside,
        fill_color: false
      },
      [
        "    ###    ",
        "   #####   ",
        "  #######  ",
        " ######### ",
        " ######### ",
        " ######### ",
        "###########",
        "###########",
        "###########",
        "###########",
        "###########",
        "###########",
        "###########",
        "###########",
        "###########",
        " ######### ",
        " ######### ",
        " ######### ",
        "  #######  ",
        "   #####   ",
        "    ###    "
      ]
    )
  end

  test "thin stroked ellipse" do
    test_ellipse(
      {20, 10},
      %PrimitiveStyle{stroke_color: true, stroke_width: 1},
      [
        "      ########      ",
        "   ###        ###   ",
        " ##              ## ",
        "##                ##",
        "#                  #",
        "#                  #",
        "##                ##",
        " ##              ## ",
        "   ###        ###   ",
        "      ########      "
      ]
    )
  end

  test "fill and stroke" do
    test_ellipse(
      {20, 10},
      %PrimitiveStyle{
        stroke_width: 3,
        stroke_color: false,
        stroke_alignment: :inside,
        fill_color: true
      },
      [
        "      ........      ",
        "   ..............   ",
        " .................. ",
        ".....##########.....",
        "...##############...",
        "...##############...",
        ".....##########.....",
        " .................. ",
        "   ..............   ",
        "      ........      "
      ]
    )
  end

  test "stroke alignment" do
    require EmbeddedGraphics.Geometry
    import EmbeddedGraphics.Geometry, only: [math_2d: 1]

    center = {15, 15}
    size = {10, 5}

    style = %PrimitiveStyle{stroke_color: true, stroke_width: 3}

    display_center = MockDisplay.new()

    Ellipse.new_with_center(center, size)
    |> Styled.new(style)
    |> Drawable.draw(display_center)

    display_inside = MockDisplay.new()

    Ellipse.new_with_center(center, math_2d(size + {2, 2}))
    |> Styled.new(%{style | stroke_alignment: :inside})
    |> Drawable.draw(display_inside)

    display_outside = MockDisplay.new()

    Ellipse.new_with_center(center, math_2d(size - {4, 4}))
    |> Styled.new(%{style | stroke_alignment: :outside})
    |> Drawable.draw(display_outside)

    assert Eq.eq?(display_inside, display_center)
    assert Eq.eq?(display_outside, display_center)
  end

  test "bounding boxes" do
    center_pt = {15, 15}
    size = {15, 25}

    style = %PrimitiveStyle{stroke_color: true, stroke_width: 3}

    center = Ellipse.new_with_center(center_pt, size) |> Styled.new(style)

    inside =
      Ellipse.new_with_center(center_pt, size)
      |> Styled.new(%{style | stroke_alignment: :inside})

    outside =
      Ellipse.new_with_center(center_pt, size)
      |> Styled.new(%{style | stroke_alignment: :outside})

    display = MockDisplay.new()
    Drawable.draw(center, display)
    assert Eq.eq?(MockDisplay.affected_area(display), Dimensions.bounding_box(center))

    display = MockDisplay.new()
    Drawable.draw(outside, display)
    assert Eq.eq?(MockDisplay.affected_area(display), Dimensions.bounding_box(outside))

    display = MockDisplay.new()
    Drawable.draw(inside, display)
    assert Eq.eq?(MockDisplay.affected_area(display), Dimensions.bounding_box(inside))
  end

  test "bounding box is independent of colors" do
    transparent_ellipse =
      Ellipse.new({5, 5}, {11, 14})
      |> Styled.new(%PrimitiveStyle{})

    filled_ellipse =
      Ellipse.new({5, 5}, {11, 14})
      |> Styled.new(%PrimitiveStyle{fill_color: true})

    assert Eq.eq?(
             Dimensions.bounding_box(transparent_ellipse),
             Dimensions.bounding_box(filled_ellipse)
           )
  end
end
