# SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0 OR MIT

# This is a port of the tests in
# https://docs.rs/embedded-graphics/0.8.1/src/embedded_graphics/primitives/line/mod.rs.html and
# https://docs.rs/embedded-graphics/0.8.1/src/embedded_graphics/primitives/line/styled.rs.html,
# which are Copyright James Waples

defmodule EmbeddedGraphics.Primitives.LineTest do
  use ExUnit.Case
  doctest EmbeddedGraphics.Primitives.Line

  alias EmbeddedGraphics.Eq
  alias EmbeddedGraphics.Drawable
  alias EmbeddedGraphics.DrawTarget.MockDisplay
  alias EmbeddedGraphics.Geometry.Dimensions
  alias EmbeddedGraphics.Primitives.Line
  alias EmbeddedGraphics.Primitives.PrimitiveStyle
  alias EmbeddedGraphics.Primitives.Rectangle
  alias EmbeddedGraphics.Primitives.Styled
  alias EmbeddedGraphics.Transform

  test "bounding box" do
    start_pt = {10, 10}
    end_pt = {19, 29}

    line = Line.new(start_pt, end_pt)
    backwards_line = Line.new(end_pt, start_pt)

    assert Eq.eq?(
             Dimensions.bounding_box(line),
             Rectangle.new(start_pt, {10, 20})
           )

    assert Eq.eq?(
             Dimensions.bounding_box(backwards_line),
             Rectangle.new(start_pt, {10, 20})
           )
  end

  @tag skip: "this test needs pixels iterators"
  test "no stroke width no line" do
  end

  test "thick line octant 1" do
    display = MockDisplay.new()

    Line.new({2, 2}, {20, 8})
    |> Styled.new(%PrimitiveStyle{stroke_color: true, stroke_width: 5})
    |> Drawable.draw(display)

    assert MockDisplay.diff_pattern(display, [
             "   #                   ",
             "  #####                ",
             "  ########             ",
             "  ###########          ",
             " ###############       ",
             "    ###############    ",
             "       ############### ",
             "          ###########  ",
             "             ########  ",
             "                #####  ",
             "                   #   "
           ]) == nil
  end

  test "thick line 2px" do
    display = MockDisplay.new()

    # Horizontal line
    Line.new({2, 2}, {10, 2})
    |> Styled.new(%PrimitiveStyle{stroke_color: true, stroke_width: 2})
    |> Drawable.draw(display)

    # Vertical line
    Line.new({2, 5}, {2, 10})
    |> Styled.new(%PrimitiveStyle{stroke_color: false, stroke_width: 2})
    |> Drawable.draw(display)

    assert MockDisplay.diff_pattern(display, [
             "            ",
             "  ######### ",
             "  ######### ",
             "            ",
             "            ",
             "  ..        ",
             "  ..        ",
             "  ..        ",
             "  ..        ",
             "  ..        ",
             "  ..        "
           ]) == nil
  end

  test "diagonal" do
    display = MockDisplay.new()

    Line.new({3, 2}, {10, 9})
    |> Styled.new(%PrimitiveStyle{stroke_color: true, stroke_width: 7})
    |> Drawable.draw(display)

    assert MockDisplay.diff_pattern(display, [
             "     #        ",
             "    ###       ",
             "   #####      ",
             "  #######     ",
             " #########    ",
             "  #########   ",
             "   #########  ",
             "    ######### ",
             "     #######  ",
             "      #####   ",
             "       ###    ",
             "        #     "
           ]) == nil
  end

  test "thick line 3px" do
    display = MockDisplay.new()

    # Horizontal line
    Line.new({2, 2}, {10, 2})
    |> Styled.new(%PrimitiveStyle{stroke_color: true, stroke_width: 3})
    |> Drawable.draw(display)

    # Vertical line
    Line.new({2, 5}, {2, 10})
    |> Styled.new(%PrimitiveStyle{stroke_color: false, stroke_width: 3})
    |> Drawable.draw(display)

    assert MockDisplay.diff_pattern(display, [
             "            ",
             "  ######### ",
             "  ######### ",
             "  ######### ",
             "            ",
             " ...        ",
             " ...        ",
             " ...        ",
             " ...        ",
             " ...        ",
             " ...        "
           ]) == nil
  end

  test "thick line 0px" do
    display = MockDisplay.new()

    Line.new({2, 2}, {2, 2})
    |> Styled.new(%PrimitiveStyle{stroke_color: true, stroke_width: 3})
    |> Drawable.draw(display)

    assert MockDisplay.diff_pattern(display, [
             "   ",
             "  #",
             "  #",
             "  #"
           ]) == nil
  end

  test "event width offset" do
    display = MockDisplay.new()

    # Horizontal line
    Line.new({2, 3}, {10, 3})
    |> Styled.new(%PrimitiveStyle{stroke_color: true, stroke_width: 4})
    |> Drawable.draw(display)

    # Vertical line
    Line.new({2, 9}, {10, 8})
    |> Styled.new(%PrimitiveStyle{stroke_color: true, stroke_width: 4})
    |> Drawable.draw(display)

    assert MockDisplay.diff_pattern(display, [
             "            ",
             "  ######### ",
             "  ######### ",
             "  ######### ",
             "  ######### ",
             "            ",
             "       #### ",
             "  ######### ",
             "  ######### ",
             "  ######### ",
             "  #####     "
           ]) == nil
  end

  # skipped: this test relies on pixels iterator, which is not public
  # test "point iter" do
  # end

  # skipped: this test relies on .perpendicular method, which is not public
  # test "perpendicular" do
  # end

  # skipped: this test relies on .extents method, which is not public
  # test "extents" do
  # end

  # skipped: this test relies on .extents method, which is not public
  # test "extents zero thickness" do
  # end

  test "styled bounding box" do
    for {line, name} <- [
          {
            Line.new({10, 20}, {10, 50}),
            "vertical"
          },
          {
            Line.new({20, 20}, {50, 20}),
            "horizontal"
          },
          {
            Line.new({20, 20}, {55, 55}),
            "diagonal"
          },
          {Line.new({20, 20}, {55, 55}), "thin"},
          {
            Line.new({40, 40}, {13, 14}),
            "random angle 1"
          },
          {
            Line.new({30, 30}, {12, 53}),
            "random angle 2"
          }
        ],
        thickness <- 1..5 do
      style = %PrimitiveStyle{stroke_color: true, stroke_width: thickness}
      styled = Styled.new(line, style)

      display = MockDisplay.new()
      Drawable.draw(styled, display)

      assert Eq.eq?(
               MockDisplay.affected_area(display),
               Dimensions.bounding_box(styled)
             ),
             "#{name}, #{thickness} px"
    end
  end

  test "bounding box is independent of colors" do
    line = Line.new({5, 5}, {11, 14})

    transparent_line = Styled.new(line, %PrimitiveStyle{stroke_width: 10})
    stroked_line = Styled.new(line, %PrimitiveStyle{stroke_color: true, stroke_width: 10})

    assert Eq.eq?(
             Dimensions.bounding_box(transparent_line),
             Dimensions.bounding_box(stroked_line)
           )
  end
end
