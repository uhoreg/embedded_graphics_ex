# SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0 OR MIT

# This is a port of the tests in
# https://docs.rs/embedded-graphics/0.8.1/src/embedded_graphics/primitives/arc/mod.rs.html and
# https://docs.rs/embedded-graphics/0.8.1/src/embedded_graphics/primitives/arc/styled.rs.html,
# which are Copyright James Waples

defmodule EmbeddedGraphics.Primitives.CircleTest do
  use ExUnit.Case
  doctest EmbeddedGraphics.Primitives.Circle

  alias EmbeddedGraphics.Eq
  alias EmbeddedGraphics.Drawable
  alias EmbeddedGraphics.DrawTarget.MockDisplay
  alias EmbeddedGraphics.Geometry.Dimensions
  alias EmbeddedGraphics.Primitives.Circle
  alias EmbeddedGraphics.Primitives.PrimitiveStyle
  alias EmbeddedGraphics.Primitives.Rectangle
  alias EmbeddedGraphics.Primitives.Styled
  alias EmbeddedGraphics.Transform

  test "negative dimensions" do
    circle = Circle.new({-15, -15}, 20)

    assert Eq.eq?(
             Dimensions.bounding_box(circle),
             Rectangle.new({-15, -15}, {20, 20})
           )
  end

  test "dimensions" do
    circle = Circle.new({5, 15}, 10)

    assert Eq.eq?(
             Dimensions.bounding_box(circle),
             Rectangle.new({5, 15}, {10, 10})
           )
  end

  test "center is correct" do
    # odd diameter
    circle = Circle.new({10, 10}, 5)
    assert Circle.center(circle) == {12, 12}

    # even diameter
    circle = Circle.new({10, 10}, 6)
    assert Circle.center(circle) == {12, 12}

    # odd diameter
    circle = Circle.new_with_center({10, 10}, 5)
    assert Circle.center(circle) == {10, 10}

    # even diameter
    circle = Circle.new_with_center({10, 10}, 6)
    assert Circle.center(circle) == {10, 10}
  end

  @tag skip: "this test needs point iterators"
  test "contains" do
    # TODO: ...
  end

  @tag skip: "offset isn't implemented yet"
  test "offset" do
    # TODO: ...
  end

  @tag skip: "this test needs pixels iterators"
  test "fill" do
    # TODO:
  end

  @tag skip: "this test needs pixels iterators"
  test "stroke" do
    # TODO:
  end

  @tag skip: "this test needs pixels iterators"
  test "stroke and fill" do
    # TODO:
  end

  @tag skip: "this test needs pixels iterators"
  test "filled styled points matches points" do
    # TODO:
  end

  test "tiny circle" do
    display = MockDisplay.new()

    Circle.new({0, 0}, 3)
    |> Styled.new(%PrimitiveStyle{stroke_color: true, stroke_width: 1})
    |> Drawable.draw(display)

    assert MockDisplay.diff_pattern(display, [" # ", "# #", " # "]) == nil
  end

  test "tiny circle filled" do
    display = MockDisplay.new()

    Circle.new({0, 0}, 3)
    |> Styled.new(%PrimitiveStyle{fill_color: true})
    |> Drawable.draw(display)

    assert MockDisplay.diff_pattern(display, [" # ", "###", " # "]) == nil
  end

  @tag skip: "this test needs pixel iterators"
  test "transparent border" do
    # TODO:
  end

  @tag skip: "this test needs pixel iterators"
  test "it handles negative coordinates" do
    # TODO:
  end

  test "stroke alignment" do
    center = {15, 15}
    size = 10

    style = %PrimitiveStyle{stroke_color: true, stroke_width: 3}

    display_center = MockDisplay.new()

    Circle.new_with_center(center, size)
    |> Styled.new(style)
    |> Drawable.draw(display_center)

    display_inside = MockDisplay.new()

    Circle.new_with_center(center, size + 2)
    |> Styled.new(%{style | stroke_alignment: :inside})
    |> Drawable.draw(display_inside)

    display_outside = MockDisplay.new()

    Circle.new_with_center(center, size - 4)
    |> Styled.new(%{style | stroke_alignment: :outside})
    |> Drawable.draw(display_outside)

    assert Eq.eq?(display_inside, display_center)
    assert Eq.eq?(display_outside, display_center)
  end

  test "issue 143 stroke and fill" do
    for size <- 0..10 do
      circle_no_stroke =
        Circle.new({10, 16}, size)
        |> Styled.new(%PrimitiveStyle{fill_color: true})

      style = %PrimitiveStyle{fill_color: true, stroke_color: true, stroke_width: 1}

      circle_stroke =
        Circle.new({10, 16}, size)
        |> Styled.new(style)

      assert Eq.eq?(
               Dimensions.bounding_box(circle_stroke),
               Dimensions.bounding_box(circle_no_stroke)
             ),
             "Filled and unfilled circle bounding boxes are unequal for radius #{size}"

      # TODO: need pixels iterator for this
      # assert!(
      #   circle_no_stroke.pixels().eq(circle_stroke.pixels()),
      #   "Filled and unfilled circle iters are unequal for radius {}",
      #   size
      # );
    end
  end

  test "bounding boxes" do
    center_pt = {15, 15}
    size = 10

    style = %PrimitiveStyle{stroke_color: true, stroke_width: 3}

    center =
      Circle.new_with_center(center_pt, size)
      |> Styled.new(style)

    inside =
      Circle.new_with_center(center_pt, size)
      |> Styled.new(%{style | stroke_alignment: :inside})

    outside =
      Circle.new_with_center(center_pt, size)
      |> Styled.new(%{style | stroke_alignment: :outside})

    display = MockDisplay.new()
    Drawable.draw(center, display)

    assert Eq.eq?(
             MockDisplay.affected_area(display),
             Dimensions.bounding_box(center)
           )

    display = MockDisplay.new()
    Drawable.draw(outside, display)

    assert Eq.eq?(
             MockDisplay.affected_area(display),
             Dimensions.bounding_box(outside)
           )

    display = MockDisplay.new()
    Drawable.draw(inside, display)

    assert Eq.eq?(
             MockDisplay.affected_area(display),
             Dimensions.bounding_box(inside)
           )
  end

  test "bounding box is independent of colors" do
    transparent_circle =
      Circle.new({5, 5}, 11)
      |> Styled.new(%PrimitiveStyle{})

    filled_circle =
      Circle.new({5, 5}, 11)
      |> Styled.new(%PrimitiveStyle{fill_color: true})

    assert Eq.eq?(
             Dimensions.bounding_box(transparent_circle),
             Dimensions.bounding_box(filled_circle)
           )
  end
end
