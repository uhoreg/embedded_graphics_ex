# SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0 OR MIT

# port of
# https://github.com/embedded-graphics/examples/blob/main/eg-next/examples/demo-analog-clock.rs,
# which is Copyright James Waples

# requires the features `["simulator", "binary_color"]`

defmodule AnalogClock do
  alias EmbeddedGraphics.Drawable
  alias EmbeddedGraphics.DrawTarget
  alias EmbeddedGraphics.Geometry.Dimensions
  alias EmbeddedGraphics.Primitives.Primitive
  alias EmbeddedGraphics.Primitives.PrimitiveStyle
  alias EmbeddedGraphics.Primitives.Circle
  alias EmbeddedGraphics.Primitives.Line
  alias EmbeddedGraphics.Primitives.Rectangle
  alias EmbeddedGraphics.Primitives.Styled
  alias EmbeddedGraphics.Primitives.StyledDrawable
  alias EmbeddedGraphics.Text.MonoText
  alias EmbeddedGraphics.Transform

  # The margin between the clock face and the display border.
  @margin 10

  # Converts a polar coordinate (angle/distance) into an (X, Y) coordinate centered around the
  # center of the circle.
  #
  # The angle is relative to the 12 o'clock position and the radius is relative to the edge of the
  # clock face.
  defp polar(circle, angle, radius_delta) do
    {x, y} = Circle.center(circle)
    radius = Circle.diameter(circle) / 2.0 + radius_delta
    {round(x + :math.sin(angle) * radius), round(y - :math.cos(angle) * radius)}
  end

  # Converts an hour into an angle in radians.
  defp hour_to_angle(hour) do
    # Convert from 24 to 12 hour time.
    hour = rem(hour, 12)

    hour / 12.0 * 2.0 * :math.pi()
  end

  # Converts a sexagesimal (base 60) value into an angle in radians.
  defp sexagesimal_to_angle(value) do
    value / 60.0 * 2.0 * :math.pi()
  end

  @doc "Creates a centered circle for the clock face."
  def create_face(target) do
    bounding_box = Dimensions.bounding_box(target)
    {width, height} = Rectangle.size(bounding_box)
    diameter = min(width, height) - 2 * @margin
    Circle.new_with_center(Rectangle.center(bounding_box), diameter)
  end

  @doc "Draws a circle and 12 graduations as a simple clock face."
  def draw_face(target, clock_face) do
    # draw the outer face
    face_style = %PrimitiveStyle{stroke_color: true, stroke_width: 2}
    StyledDrawable.draw_styled(clock_face, face_style, target)

    # Draw 12 graduations.
    tick_style = %PrimitiveStyle{stroke_color: true, stroke_width: 1}

    Enum.each(0..11, fn hour ->
      angle = hour_to_angle(hour)
      # Start point on circumference.
      start_point = polar(clock_face, angle, 0)
      # End point offset by 10 pixels from the edge.
      end_point = polar(clock_face, angle, -10)

      Line.new(start_point, end_point)
      |> StyledDrawable.draw_styled(tick_style, target)
    end)
  end

  @doc "Draws a clock hand."
  def draw_hand(target, clock_face, angle, length_delta) do
    center = Circle.center(clock_face)
    end_point = polar(clock_face, angle, length_delta)

    Line.new(center, end_point)
    |> StyledDrawable.draw_styled(
      %PrimitiveStyle{stroke_color: true, stroke_width: 1},
      target
    )
  end

  @doc "Draws a decorative circle on the second hand."
  def draw_second_hand_decoration(target, clock_face, angle, length_delta) do
    decoration_position = polar(clock_face, angle, length_delta)

    style = %PrimitiveStyle{fill_color: false, stroke_color: true, stroke_width: 1}

    Circle.new_with_center(decoration_position, 11)
    |> StyledDrawable.draw_styled(style, target)
  end

  @doc "Draw digital clock just above center with black text on a white background"
  def draw_digital_clock(target, clock_face, time_str) do
    # Create a styled text object for the time text.
    {:ok, style} = MonoText.Style.new(false, :font9x15_ascii)

    text =
      MonoText.new(
        time_str,
        {0, 0},
        style
      )

    # Move text to be centered between the 12 o'clock point and the center of the clock face.
    text_dimensions = Dimensions.bounding_box(text)
    {clock_face_cx, clock_face_cy} = Circle.center(clock_face)
    {text_cx, text_cy} = Rectangle.center(text_dimensions)
    {_, height} = clock_face |> Dimensions.bounding_box() |> Rectangle.size()

    text =
      Transform.translate(
        text,
        {clock_face_cx - text_cx, clock_face_cy - text_cy - round(height / 4)}
      )

    # Add a background around the time digits.
    # Note that there is no bottom-right padding as this is added by the font renderer itself.
    text_dimensions = Dimensions.bounding_box(text)
    {left, top} = Rectangle.top_left(text_dimensions)
    {width, height} = Rectangle.size(text_dimensions)

    Rectangle.new({left - 3, top - 3}, {width + 4, height + 4})
    |> StyledDrawable.draw_styled(
      %PrimitiveStyle{fill_color: true},
      target
    )

    # Draw the text after the background is drawn.
    Drawable.draw(text, target)
  end

  def run(target, clock_face) do
    EmbeddedGraphics.DrawTarget.clear(target, false)

    {{_, _, _}, {hours, minutes, seconds}} = :calendar.local_time()
    hours_radians = hour_to_angle(hours)
    minutes_radians = sexagesimal_to_angle(minutes)
    seconds_radians = sexagesimal_to_angle(seconds)

    pad = &(&1 |> to_string() |> String.pad_leading(2, "0"))
    digital_clock_text = "#{pad.(hours)}:#{pad.(minutes)}:#{pad.(seconds)}"

    draw_face(target, clock_face)
    draw_hand(target, clock_face, hours_radians, -60)
    draw_hand(target, clock_face, minutes_radians, -30)
    draw_hand(target, clock_face, seconds_radians, -0)
    draw_second_hand_decoration(target, clock_face, seconds_radians, -20)

    # Draw digital clock just above center.
    draw_digital_clock(target, clock_face, digital_clock_text)

    # Draw a small circle over the hands in the center of the clock face.
    # This has to happen after the hands are drawn so they're covered up.
    Circle.new_with_center(Circle.center(clock_face), 9)
    |> Primitive.styled(%PrimitiveStyle{fill_color: true})
    |> Drawable.draw(target)

    EmbeddedGraphics.DrawTarget.SimulatorDisplay.update_window(target)

    # FIXME: check window events to see if it gets closed

    :timer.sleep(50)

    run(target, clock_face)
  end
end

display = EmbeddedGraphics.DrawTarget.SimulatorDisplay.new({256, 256}, "Clock")
clock_face = AnalogClock.create_face(display)

AnalogClock.run(display, clock_face)
