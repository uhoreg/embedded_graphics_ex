# EmbeddedGraphics

Wrapper for the [embedded-graphics](https://crates.io/crates/embedded-graphics)
crate.

To use this package, you will need to choose a display driver and a color type
(if the display driver supports multiple color types).  This can be configured
by setting the Rust crate features in `config/config.exs`.  For example:

```elixir
import Config

config :embedded_graphics, EmbeddedGraphics.Nif,
  default_features: false,  # use the features listed below instead of the default ones
  features: ["simulator", "binary_color"]
```

uses the embedded-graphics simulator display driver with `BinaryColor` color
type (e.g. black-and-white).  The default features are `["simulator",
"binary_color"]`.

The supported display drivers currently are:

- [`embedded-graphics-simulator`](https://crates.io/crates/embedded-graphics-simulator),
  which displays on a window (feature: `simulator`)
- [`mock_display`](https://docs.rs/embedded-graphics/0.8.1/embedded_graphics/mock_display/index.html)
  which creates a small display for testing (feature: `mock`)

The supported color types currently are:

- [`BinaryColor`](https://docs.rs/embedded-graphics/0.8.1/embedded_graphics/pixelcolor/enum.BinaryColor.html),
  which represents two color states (such as black-and-white)

Note that this means that the package can only support one display type and one
color type at once.  Also, since this is configured at compile-time, we cannot
provide pre-compiled libraries.  Rustler provides [configuration
options](https://hexdocs.pm/rustler/Rustler.html) to control how the library is
built, such as specifying how to find the Rust compiler.

The API provided tries to closely follow the API from the embedded-graphics
crate, but also tries to make objects immutable whenever possible, to match
Erlang semantics.

Rust traits are mapped to Elixir protocols.  For example, the
[`embedded_graphics::primitives::StyledDrawable`](https://docs.rs/embedded-graphics/0.8.1/embedded_graphics/primitives/trait.StyledDrawable.html)
trait corresponds to the `EmbeddedGraphics.Primitives.StyledDrawable`
protocol, so a Rust statement of

```rust
rectangle.draw_styled(&style, &mut display_target)
```

would be performed by doing

```elixir
EmbeddedGraphics.Primitives.StyledDrawable.draw_styled(rectangle, style, display_target)
```

or

```elixir
rectangle
|> EmbeddedGraphics.Primitives.StyledDrawable.draw_styled(style, display_target)
```

in Elixir.

## Examples

The `examples` directory contains examples scripts, ported from the
[embedded-graphics Rust crate
examples](https://github.com/embedded-graphics/examples/tree/main/eg-next/examples).
The ports are fairly literal, to allow for easier comparison between the code.
The examples all use the embedded-graphics simulator, and a comment at the top
will indicate which color type it uses.

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `embedded_graphics` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:embedded_graphics, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at <https://hexdocs.pm/embedded_graphics>.

## License

This package may be used under the MIT or Apache-2.0 licenses, the same
licenses used by the embedded-graphics crate.

```text
SPDX-FileCopyrightText: Hubert Chathi <hubert@uhoreg.ca>
SPDX-License-Identifier: Apache-2.0 OR MIT
```
